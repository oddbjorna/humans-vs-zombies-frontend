# Humans vs. Zombies

Humans vs. Zombies (HvZ) is a game of tag played at schools, camps, neighborhoods, libraries, and conventions around the world.
The game simulates the exponential spread of a fictional zombie infection through a population.
This application creates a base for playing HvZ in the real world where the players can manage their own states. They can see whether they are humans or zombies, join a squad of other humans or zombies depending on faction, leave check-in markers for your squad to show where you are on the map. 

The backend source code with more details about the application can be found at: [https://gitlab.com/oddbjorna/HvZ-api]

## Usage

The application can be tested at: [https://humans-vs-zombies-react.herokuapp.com/login].

A user manual is found at: [https://gitlab.com/oddbjorna/humans-vs-zombies-frontend/-/blob/master/HvZ_user_manual.pdf].

## Contributors:

Oddbjørn Almenning, 
Ludvig Ånestad, 
Sunniva Stolt-Nielsen, 
Nanfrid Idsø

## Pages:

### Login page

The Login Page presents the user with the name of the application and a login button. By clicking the login button the user is redirected to keycloak where they can login if they have an existing user or register as a new user.

### Landing page

The Landing Page presents the user with the game rules for HvZ and a list of gamecards. Each gamecard containes the gamename, gamestate and amount of players currently in the game. By clicking on a gamecard the user can see description, a list of squads currently in the game, and a "join game" button. A map is also presented showing the  game area and gravestones.

A game can be in three states, "Registration", "In progress", or "Complete". Users can join a game when it is in the registration state, but when the game is in progress the game can't be joiined and the gamecard becomes unclickable. Once the game is complete it disappears from the game list. The Admin user controls what gamestate each game is in.

### Game page

When a user joins a game, a player is created. On the Game Page the player is placed in a "lobby" when the game is in registration. When the game is in progress, the player is presented with a map showing the game area and any gravestones and mission markers that may have been placed.
If the player is human they will have the "View BiteCode" button which will show a uniqe code that the human player will have to show a zombie player if they are bitten. If the player is a zombie, a "Register kill" button has replaced the "View Bitecode" button, allowing the player to write a humans bitecode to turn them into a zombie. On this page either faction (human or zombie) can join or create squads using the "Join Squad" button. 

If a player is in a squad they can leave a "Squad Chekin" to show their squad members where they are on the map. A new button "Squad Details" will also appear, which contains information on how many squad members have turned into zombies, each squad members rank, and their status.

The Game Page also has a chat, which is accessed from the menu in the navbar. The chat is divided into "Global", "Squad", and "Faction". The Global chat can be viewed by all players in the game, the squad chat is only accessable when the player is in a squad, and the faction chat is only available for players in the same faction.

### Profile Page

The Profile Page shows which faction the player is in, their username and rank. If the user is not in a game these details will not show.

### Admin Profile

The Admin Profile page contains multiple sections divided into four features.

* Admin Home  
Admin Home gives Admin a list of games, a map and chat for the game they have joined. Here, Admin can write in any chat in any game, and see the gravestones for each game.

* Game 
The Game page provides Admin with the ability to create new games with a map to set the game area. A list of existing games is also provided and the ability to change the state of existing games. 

* Mission
The Mission page provides Admin with the ability to create new missions with a map to set the location for the mission. A game list and mission list is also provided, as well as the ability to change the description of an existing mission.

* Player
The Player page provides Admin with a player list with each players username, ID, game ID and state. This page also allows Admin to change whether a player is a human or a zombie. 

## Limitations:

### Map Markers
Currently all markers can be placed outside the play area, these should prefferably be restricted to play area only.
Admin can not see squad checkin markers without being part of the respective squad.

### Admin Squad Chat
Currently Admin cannot post in the squad chat from admin view, they would have to join a game and then join a squad.

## Installation
Frontend installation:

Clone the source code: git clone https://gitlab.com/oddbjorna/humans-vs-zombies-frontend

Move to the project folder: cd humans-vs-zombies-frontend

Install dependencies: npm install

Run the app in the development mode: npm start

Open http://localhost:3000 to view it in the browser.
