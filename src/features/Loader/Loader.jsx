import styles from "./Loader.module.css";

/**
 * Loader Component that is displayed when a page or component is loading.
 */
const Loader = (props) => {
  return (
    <div
      className={styles.container}
      style={{ height: props.height, width: props.width }}
    >
      <div className={styles.ldsring}>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  );
};

export default Loader;
