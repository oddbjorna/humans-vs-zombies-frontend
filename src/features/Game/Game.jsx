import styles from "./Game.module.css";
import Chat from "../Chat/Chat";
import withKeycloak from "../../hoc/withKeycloak";
import Map from "../Map/Map";
import { useState, useEffect } from "react";
import JoinSquad from "./JoinSquad";
import RegisterKill from "./RegisterKill";
import { useDispatch, useSelector } from "react-redux";
import {
  sessionSetAction,
  sessionSuccessAction,
  sessionUpdateAction,
  sessionUpdateRankAction
} from "../../store/actions/sessionActions";
import { squadSetAction } from "../../store/actions/squadActions";
import { killSetAction } from "../../store/actions/killActions";
import APIService from "../../services/APIService";
import SquadDetails from "./SquadDetails";
import Menu from "../Navbar/Menu";
import { useHistory } from "react-router-dom";
import { playerSetAction } from "../../store/actions/playerActions";
import { gameSetAction } from "../../store/actions/gameActions";
import BiteCode from "./BiteCode";
import { connectionSetAction } from "../../store/actions/connectionActions";
import Loader from "../Loader/Loader";
import { missionSetAction } from "../../store/actions/missionActions";
/**
 * Game Page which is displayed to the user after they have joined a game.
 */
const Game = (props) => {
  //Game Variables
  //const [connection, setConnection] = useState(null);
  const [inSquad, setInSquad] = useState(false);
  const [active, setActive] = useState(false);
  const [details, setDetails] = useState(false);
  const [checkin, setCheckin] = useState({});
  const [chatActive, setChatActive] = useState(false);
  const [killActive, setKillActive] = useState(false);
  const [killer, setKiller] = useState({});
  const [zombie, setZombie] = useState(false);
  const [biteCodeActive, setBiteCodeActive] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  const user = JSON.parse(localStorage.getItem("user"));

  const { connection } = useSelector((state) => state.connectionReducer);
  const { game } = useSelector((state) => state.gameReducer);

  const dispatch = useDispatch();
  const history = useHistory();
  const { player } = useSelector((state) => state.playerReducer);
  const { kill } = useSelector((state) => state.killReducer);

  //Function that runs when a player leaves a squad
  const handleLeave = async () => {
    dispatch(sessionUpdateAction());
    dispatch(squadSetAction());
    await connection.send("SendCheckin", { marker: "message" });
    setInSquad(false);
  };

  //Function that runs when a user is in a squad
  const handleInSquad = () => {
    dispatch(squadSetAction());
    setInSquad(true);
  };

  //Functions used show show/hide different components
  const handleActive = () => {
    if (active) {
      setActive(false);
    } else {
      setActive(true);
    }
  };

  const handleChatActive = () => {
    if (chatActive) {
      setChatActive(false);
    } else {
      setChatActive(true);
    }
  };

  const handleDetailsActive = () => {
    if (details) {
      setDetails(false);
    } else {
      setDetails(true);
      dispatch(squadSetAction());
    }
  };

  const handleKill = () => {
    if (killActive) {
      setKillActive(false);
    } else {
      setKillActive(true);
      dispatch(killSetAction());
    }
  };

  const handleBiteCode = () => {
    if (biteCodeActive) {
      setBiteCodeActive(false);
    } else {
      setBiteCodeActive(true);
    }
  };

  /**
   * Function that runs when the user tries to post a squad check-in
   * runs the postCheckin method from the APIService
   * pings the server
   */
  const handleSquadCheckinClick = async () => {
    await APIService.postSquadCheckin(checkin);
    await connection.send("SendCheckin", { marker: "message" });
    dispatch(squadSetAction());
  };

  /**
   * Function that runs when a Zombie Tries to kill a Human
   * Takes the Human BiteCode as parameter
   * Checks if the vicitim is a Human and if the Bitecode is correct
   * Checks how many kills a player have and increases their rank if they number corresponds with a value in the numbers array
   * dispatches the sessionUpdateRankAction from redux
   * if the human exists:
   * runs the postkill method from the apiService with the killer and bitecode as parameters
   * pings the server
   * dispatches the killSetAction from redux
   */
  const handleKillClick = async (biteCode) => {
    const humans = player.filter((p) => p.isHuman === true);
    const exists = humans.filter((h) => h.biteCode === biteCode);

    const user = JSON.parse(localStorage.getItem("user"));
    const playerKills = kill.filter((k) => k.killerId === user.playerId);
    const numbers = [1, 3, 5, 7];
    if (numbers.includes(playerKills.length)) {
      await APIService.increaseRank(user.playerId);
      dispatch(sessionUpdateRankAction());
    }
    if (exists.length) {
      await APIService.postKill(killer, biteCode);
      await connection.send("SendKill", { killMessage: biteCode });
      dispatch(killSetAction());
    }
  };

  /**
   * Function that runs after a Player is killed and turns the into a Zombie
   * Updates the number of deceased players in the victims squad by 1
   * Kicks the victim out of their squad
   * sets BiteCodeActive to false so that the player cant view their bitecode anymore.
   */
  const handleKillReceived = async (victim) => {
    const newPlayers = await APIService.getPlayers();
    const targetPlayer = newPlayers.filter(
      (p) => p.biteCode === victim.killMessage
    )[0];
    const user = JSON.parse(localStorage.getItem("user"));
    APIService.increaseDeceasedSquad(user.squadId);
    if (
      targetPlayer !== undefined &&
      targetPlayer.userId === JSON.parse(localStorage.getItem("user")).userId
    ) {
      await handleLeave();
      await APIService.setZombie(targetPlayer.id);
      setTimeout(() => dispatch(sessionSetAction()), 500);
      setZombie(true);
      setBiteCodeActive(false);
    }
    dispatch(squadSetAction());
    dispatch(killSetAction());
  };

  /**
   * Redirects the user back to dashboard if theres no object in localstorage
   * if the user is in a squad - sets the inSquad to true
   * dispatches severel setActions on render
   */
  useEffect(() => {
    const user = JSON.parse(localStorage.getItem("user"));
    if (user === null) {
      history.push("/dashboard");
    }
    if (user && user.squadId) {
      setInSquad(true);
    }
    dispatch(squadSetAction());
    dispatch(sessionSuccessAction(user));
    dispatch(killSetAction());
    dispatch(missionSetAction());
    dispatch(playerSetAction());
    dispatch(gameSetAction());

    dispatch(connectionSetAction());
    setIsLoading(false);
  }, []);

  //Initializes connection to websockets
  useEffect(() => {
    if (!isLoading) {
      if (connection) {
        connection
          .start()
          .then((result) => {
            console.log("Connected to gameHub!");

            connection.on("ReceiveMarker", (message) => {
              dispatch(squadSetAction());
            });
            connection.on("ReceiveKill", (victim) => {
              dispatch(playerSetAction());
              handleKillReceived(victim);
            });
            connection.on("ReceiveMission", (mission) => {
              dispatch(missionSetAction());
            });
          })
          .catch((e) => console.log("Connection failed", e));
      }
      return () => {
        if (connection) {
          connection.stop().then(() => {
            console.log("stopped");
          });
        }
      };
    }
  }, [connection]);

  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <div className={styles.container}>
          <div className={styles.imgContainer}>
            <div className={styles.main}>
              <div className={styles.gameHeader}>
                <h1 className={styles.gameTitle}>
                  You are currently playing {user.gameName}!
                </h1>
                {zombie || user.zombie ? (
                  <h3>You are a zombie.</h3>
                ) : (
                  <h3>You are a human.</h3>
                )}
              </div>

              {game.map((g) => {
                return g.gameState === "Registration" &&
                  g.id === user.gameId ? (
                  <h1 className={styles.lobbyMessage}>
                    Please wait for the game to start!
                  </h1>
                ) : g.gameState === "Complete" && g.id === user.gameId ? (
                  <h1 className={styles.completeMessage}>
                    This game is finished.
                  </h1>
                ) : g.id === user.gameId && g.gameState === "In Progress" ? (
                  <Map
                    checkin={checkin}
                    connection={connection}
                    handleCheckin={setCheckin}
                    handleKill={setKiller}
                  />
                ) : (
                  ""
                );
              })}

              <div className={styles.buttons}>
                {inSquad ? (
                  <button
                    className={`${styles.btn} ${styles.leave}`}
                    onClick={handleLeave}
                  >
                    Leave Squad
                  </button>
                ) : (
                  <button
                    className={styles.btn}
                    onClick={() => {
                      setActive(true);
                    }}
                  >
                    Join Squad
                  </button>
                )}
                {active && (
                  <JoinSquad
                    active
                    handleActive={handleActive}
                    handleInSquad={handleInSquad}
                  />
                )}
                {details && (
                  <SquadDetails
                    details
                    handleDetailsActive={handleDetailsActive}
                  />
                )}
                {inSquad && (
                  <button className={styles.btn} onClick={handleDetailsActive}>
                    Squad Details
                  </button>
                )}
                {inSquad && (
                  <button
                    className={styles.btn}
                    onClick={handleSquadCheckinClick}
                  >
                    Squad Checkin
                  </button>
                )}
                {killActive && (
                  <RegisterKill
                    killActive
                    handleKill={handleKill}
                    handleKillClick={handleKillClick}
                  />
                )}
                {zombie || JSON.parse(localStorage.getItem("user")).zombie ? (
                  game.map((g) => {
                    return g.gameState === "Registration" &&
                      g.id === user.gameId ? (
                      <div className={styles.disableBtn}>
                        <button className={styles.btn} onClick={handleKill}>
                          Register Kill
                        </button>
                      </div>
                    ) : g.id === user.gameId &&
                      g.gameState === "In Progress" ? (
                      <button className={styles.btn} onClick={handleKill}>
                        Register Kill
                      </button>
                    ) : (
                      ""
                    );
                  })
                ) : (
                  <button className={styles.btn} onClick={handleBiteCode}>
                    View BiteCode
                  </button>
                )}
              </div>

              {active && (
                <JoinSquad
                  active
                  handleActive={handleActive}
                  handleInSquad={handleInSquad}
                />
              )}
              {details && (
                <SquadDetails
                  details
                  handleDetailsActive={handleDetailsActive}
                />
              )}

              {killActive && (
                <RegisterKill
                  killActive
                  handleKill={handleKill}
                  handleKillClick={handleKillClick}
                />
              )}
              {biteCodeActive && <BiteCode handleBiteCode={handleBiteCode} />}
            </div>
          </div>
          {chatActive && (
            <Chat inGame={true} inSquad={inSquad} connection={connection} />
          )}

          <Menu
            connection={connection}
            handleLeave={handleLeave}
            handleChatActive={handleChatActive}
            handleSquadActive={handleActive}
            handleDetailsActive={handleDetailsActive}
            handleKill={handleKill}
            handleBiteCode={handleBiteCode}
          />
        </div>
      )}
    </>
  );
};
export default withKeycloak(Game);
