import styles from "./RegisterKill.module.css";
import CloseIcon from "@mui/icons-material/Close";
import { useDispatch } from "react-redux";
import { killSetAction } from "../../store/actions/killActions";
import { useEffect, useState } from "react";
import { playerSetAction } from "../../store/actions/playerActions";

/**
 * Component that is displayed when a Zombie registers a kill
 */
const RegisterKill = (props) => {
  //Register Kill Variables
  const dispatch = useDispatch();
  const [biteCode, setBiteCode] = useState("");

  //Function that runs when a Zombie Submits a kill
  const handleSubmit = (e) => {
    e.preventDefault();
    props.handleKillClick(biteCode);
    document.getElementById("form").reset();
    props.handleKill();
  };

  // Dispatches different setActions from redux
  useEffect(() => {
    dispatch(playerSetAction());
    dispatch(killSetAction());
  }, []);

  return (
    <div className={styles.container}>
      <div className={styles.formContainer}>
        <button className={styles.close} onClick={props.handleKill}>
          <CloseIcon />
        </button>
        <form onSubmit={handleSubmit} id="form">
          <h2>Enter bitecode</h2>
          <input
            className={styles.input}
            type="text"
            onChange={(e) => setBiteCode(e.target.value)}
          />
          <button className={styles.button} type="submit">
            Register Kill
          </button>
        </form>
      </div>
    </div>
  );
};

export default RegisterKill;
