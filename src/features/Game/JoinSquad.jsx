import styles from "./JoinSquad.module.css";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { squadSetAction } from "../../store/actions/squadActions";
import CloseIcon from "@mui/icons-material/Close";
import { sessionSetSquadAction } from "../../store/actions/sessionActions";
import { useState } from "react";
import APIService from "../../services/APIService";

/**
 * Compenent where a user can create or join and exisiting squad
 */
const JoinSquad = (props) => {
  //JoinSquad variables
  const { squad } = useSelector((state) => state.squadReducer);
  const user = JSON.parse(localStorage.getItem("user"));
  const [newSquad, setNewSquad] = useState(false);
  const dispatch = useDispatch();
  const [input, setInput] = useState(false);

  //function that runs when a player joins a squad, dispatches the sessionSetSquadAction
  const makeSquadMember = async (id) => {
    dispatch(sessionSetSquadAction({ id }));
  };

  useEffect(() => {
    dispatch(squadSetAction());
  }, []);

  const handleNewSquad = () => {
    setNewSquad(false);
  };
  //onClick function that runs when the create squad button is clicked
  function handleSubmit() {
    createSquad(input);
  }

  /**
   * Method that runs when a player creates a new squad
   * Runs the postSquad method from APIservice with suqad name as parameter
   * dispatches the squadSetAction from redux
   */
  const createSquad = async (squadName) => {
    await APIService.postSquad(squadName);
    dispatch(squadSetAction());
  };
  return (
    <div className={styles.container}>
      <div className={styles.formContainer}>
        <button className={styles.close} onClick={props.handleActive}>
          <CloseIcon />
        </button>
        <ul className={styles.squadList}>
          {newSquad ? (
            <form className="form">
              <input
                autoComplete="off"
                type="text"
                placeholder="Enter squad name"
                onChange={(e) => setInput(e.target.value)}
                className={styles.input}
                required={true}
              />

              <button
                type="submit"
                className={`${styles.button} ${styles.submit}`}
                onClick={() => {
                  handleSubmit();
                  handleNewSquad();
                }}
              >
                Create Squad
              </button>
            </form>
          ) : (
            squad.map((s) => {
              return s.gameId === user.gameId && s.isHuman === user.human ? (
                <li
                  key={s.id}
                  className={styles.squad}
                  onClick={() => {
                    if (s.squadMembers.length >= 6) {
                      alert("This squad is full");
                    } else {
                      makeSquadMember(s.id);
                      props.handleInSquad();
                      props.handleActive();
                    }
                  }}
                >
                  <div className={styles.squadInfo}>
                    <h1>{s.name}</h1>
                    <h4>{s.squadMembers.length} / 6</h4>
                    <h4>{"Squad members: " + s.squadMembers.length}</h4>
                    {user.human ? <h4>Deceased: {s.deceased}</h4> : ""}
                  </div>
                </li>
              ) : (
                ""
              );
            })
          )}
        </ul>
        {!newSquad ? (
          <button
            className={styles.button}
            onClick={() => {
              setNewSquad(true);
            }}
          >
            Create new Squad
          </button>
        ) : (
          <span></span>
        )}
      </div>
    </div>
  );
};

export default JoinSquad;
