import styles from "./BiteCode.module.css";
import CloseIcon from "@mui/icons-material/Close";
import { useSelector } from "react-redux";

/**
 * BiteCode Component that displays your unique bitecode
 */
const BiteCode = (props) => {
  const { player } = useSelector((state) => state.playerReducer);
  const user = JSON.parse(localStorage.getItem("user"));
  return (
    <div className={styles.container}>
      <div className={styles.formContainer}>
        <button className={styles.close} onClick={props.handleBiteCode}>
          <CloseIcon />
        </button>
        {player.map((p) => {
          return p.id === user.playerId ? (
            <div key={p.id}>
              <h1>Your BiteCode:</h1>
              <h1>{p.biteCode}</h1>
            </div>
          ) : (
            ""
          );
        })}
      </div>
    </div>
  );
};

export default BiteCode;
