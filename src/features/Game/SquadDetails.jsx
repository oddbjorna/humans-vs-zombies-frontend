import styles from "./JoinSquad.module.css";
import { useSelector } from "react-redux";
import CloseIcon from "@mui/icons-material/Close";
/**
 * Component that is displayed when the Player clicks the squad details button.
 * Shows additional information about the selected squad.
 */
const SquadDetails = (props) => {
  const { squad, squadMembers } = useSelector((state) => state.squadReducer);
  const user = JSON.parse(localStorage.getItem("user"));
  return (
    <div className={styles.container}>
      <div className={styles.formContainer}>
        <button className={styles.close} onClick={props.handleDetailsActive}>
          <CloseIcon />
        </button>
        <ul>
          {squad.map((s) => {
            return s.id === user.squadId ? (
              <li key={s.id} className={styles.details}>
                <div>
                  <div className={styles.header}>
                    <h1>{s.name}</h1>
                    {user.human && (
                      <h4 className={styles.deceased}>
                        Deceased: {s.deceased}
                      </h4>
                    )}
                  </div>
                  <h3>Squad Members:</h3> <br />
                  {squadMembers.map((sm) => {
                    return sm.squadId === user.squadId ? (
                      <div key={sm.id}>
                        <h4>{sm.username + " rank: " + user.rank}</h4>

                        <h4>{sm.isHuman ? "Alive" : "Deceased"}</h4>
                        <br />
                      </div>
                    ) : (
                      ""
                    );
                  })}
                </div>
              </li>
            ) : (
              ""
            );
          })}
        </ul>
      </div>
    </div>
  );
};

export default SquadDetails;
