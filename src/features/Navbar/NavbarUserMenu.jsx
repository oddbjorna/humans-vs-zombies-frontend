import { NavLink } from "react-router-dom";
import KeycloakService from "../../services/KeycloakService";
import styles from "./Navbar.module.css";
import { useEffect } from "react";
import { useSelector } from "react-redux";
/**
 * Navbar Component used to navigate to Dashboard, profile/adminprofile and Game
 */
const NavbarUserMenu = () => {
  const role = KeycloakService.hasRole(["admin"]);
  const { session } = useSelector((state) => state.sessionReducer);

  useEffect(() => {}, []);

  return (
    <>
      <ul className={`${styles.NavbarMenu} ${styles.NavbarMenuRight}`}>
        <li className={styles.NavbarMenuItem}>
          <NavLink
            id={styles.dashboard}
            className="material-icons"
            activeClassName={styles.NavbarActive}
            to="/dashboard"
          ></NavLink>
        </li>
        <li className={styles.NavbarMenuItem}>
          <NavLink
            id={styles.profile}
            className="material-icons"
            activeClassName={styles.NavbarActive}
            to={role ? "/admin" : "/profile"}
          ></NavLink>
        </li>
        {session === null ? (
          ""
        ) : session.gameId ? (
          <li className={styles.NavbarMenuItem}>
            <NavLink
              id={styles.game}
              className="material-icons"
              activeClassName={styles.NavbarActive}
              to="/game"
            ></NavLink>
          </li>
        ) : (
          ""
        )}
      </ul>
    </>
  );
};
export default NavbarUserMenu;
