import styles from './Menu.module.css';
import KeycloakService from '../../services/KeycloakService';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import APIService from '../../services/APIService';
import { useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import AddIcon from '@mui/icons-material/Add';
import GroupsIcon from '@mui/icons-material/Groups';
import PowerSettingsNewIcon from '@mui/icons-material/PowerSettingsNew';
import SmsOutlinedIcon from '@mui/icons-material/SmsOutlined';
import ClearRoundedIcon from '@mui/icons-material/ClearRounded';
import ListAltOutlinedIcon from '@mui/icons-material/ListAltOutlined';
import { useDispatch } from 'react-redux';
import { sessionClearAction } from '../../store/actions/sessionActions';
import TagRoundedIcon from '@mui/icons-material/TagRounded';

/**
 * Menu Component
 */
const Menu = (props) => {
  //Menu Variables
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();
  const [trigger, setTrigger] = useState(false);
  const user = JSON.parse(localStorage.getItem('user'));
  //onClick function for Logging out
  const handleLogoutClick = () => {
    if (window.confirm('Are you sure you want to log out?')) {
      handleLeaveGameClick();
      KeycloakService.doLogout();
    }
  };

  //Onclick function which makes the user leave their currently joined game
  const handleLeaveClick = () => {
    if (user) {
      if (
        window.confirm(
          'Are you sure you want to leave the game? Your progress will be lost'
        )
      ) {
        handleLeaveGameClick();
      }
    }
  };

  //Functions to display/hide components
  const handleChatActive = () => {
    props.handleChatActive();
    handleTriggerClick();
  };

  const handleDetailsActive = () => {
    props.handleDetailsActive();
    handleTriggerClick();
  };

  const handleTriggerClick = (e) => {
    if (trigger) {
      setTrigger(false);
    } else {
      setTrigger(true);
    }
  };

  const handleKillClick = () => {
    props.handleKill();
    handleTriggerClick();
  };

  const handleBiteCode = () => {
    props.handleBiteCode();
    handleTriggerClick();
  };

  const handleJoinSquad = () => {
    const user = JSON.parse(localStorage.getItem('user'));
    if (!user.squadId) {
      props.handleSquadActive();
      handleTriggerClick();
    }
  };

  const handleLeaveSquad = () => {
    props.handleLeave();
    handleTriggerClick();
  };

  /**
   * Onclick function for Leaving game
   * Runs the deleteSquadMember method from the APIservice
   * Runs the DeletePlayer method from the APIservice
   * Clears localStorage
   * Dispatches the SessionClearAction from redux
   * Redirects the user to the dashboard
   */
  const handleLeaveGameClick = async () => {
    await APIService.deleteSquadMember(user.playerId);
    await APIService.deletePlayer(user.playerId);

    localStorage.clear();
    dispatch(sessionClearAction());
    history.push('/dashboard');
  };

  return (
    <menu className={trigger ? styles.open : ''}>
      <span onClick={handleLogoutClick} className={styles.action}>
        <i>
          <PowerSettingsNewIcon />
        </i>
      </span>
      {user !== null ? (
        <span onClick={handleLeaveClick} className={styles.action}>
          <i>
            <ExitToAppIcon />
          </i>
        </span>
      ) : (
        <span className={styles.action} style={{ visibility: 'hidden' }}></span>
      )}
      {location.pathname !== '/dashboard' && location.pathname !== '/profile' && (
        <span onClick={handleChatActive} className={styles.action}>
          <i>
            <SmsOutlinedIcon />
          </i>
        </span>
      )}

      {location.pathname !== '/dashboard' && location.pathname !== '/profile' && (
        <span
          onClick={user.zombie ? handleKillClick : handleBiteCode}
          className={styles.action}
        >
          <i>
            {user.zombie ? (
              <img alt="skull" className={styles.death} src="/skull.svg" />
            ) : (
              <TagRoundedIcon />
            )}
          </i>
        </span>
      )}

      {location.pathname !== '/dashboard' && location.pathname !== '/profile' && (
        <span
          onClick={user && user.squadId ? handleLeaveSquad : handleJoinSquad}
          className={styles.action}
        >
          <i>{user && user.squadId ? <ClearRoundedIcon /> : <GroupsIcon />}</i>
        </span>
      )}
      {location.pathname !== '/dashboard' &&
        location.pathname !== '/profile' &&
        (user && user.squadId ? (
          <span onClick={handleDetailsActive} className={styles.action}>
            <i>
              <ListAltOutlinedIcon />
            </i>
          </span>
        ) : (
          <span
            className={styles.action}
            style={{ visibility: 'hidden' }}
          ></span>
        ))}

      <span className={styles.trigger} onClick={handleTriggerClick}>
        <i>
          <AddIcon />
        </i>
      </span>
    </menu>
  );
};
export default Menu;
