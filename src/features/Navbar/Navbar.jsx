import KeycloakService from "../../services/KeycloakService";
import NavbarUserMenu from "./NavbarUserMenu";
import styles from "./Navbar.module.css";

const Navbar = () => {
  const isLoggedIn = KeycloakService.isLoggedIn();

  return (
    <nav className={styles.Navbar}>
      <ul className={styles.NavbarMenu}>
        <li className={`${styles.NavbarMenuItem} ${styles.NavbarBrand}`}>
          {/* <h4>Humans VS Zombies</h4> */}
        </li>
      </ul>
      {isLoggedIn && <NavbarUserMenu />}
    </nav>
  );
};
export default Navbar;
