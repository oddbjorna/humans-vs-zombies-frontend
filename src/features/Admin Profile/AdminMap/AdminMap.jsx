import { useState, useEffect } from "react";
import {
  MapContainer,
  TileLayer,
  Marker,
  Popup,
  useMapEvents,
  useMap,
  Pane,
  Rectangle,
} from "react-leaflet";
import L from "leaflet";
import styles from "./AdminMap.module.css";
import Loader from "../../Loader/Loader";
import { useDispatch, useSelector } from "react-redux";
import { positionSetAction } from "../../../store/actions/positionActions";
import { useLocation } from "react-router-dom";

/**
 * Map Component for AdminProifle
 */
const AdminMap = (props) => {
  //State variables
  const [isLoading, setIsLoading] = useState(true);
  const [selectedPosition, setSelectedPosition] = useState([0, 0]);

  //Gets position values from positionreducer
  const { nwlat, nwlng, selat, selng } = useSelector(
    (state) => state.positionReducer
  );

  const dispatch = useDispatch();
  const location = useLocation();

  //Variable that contains the Green marker icon for map interaction
  const greenIcon = new L.Icon({
    iconUrl:
      "https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png",
    shadowUrl:
      "https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png",
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41],
  });

  /**
   * Function that runs when the "Set NW" button is clicked
   * Dispatches the positionSetAction from redux
   * sets the nwlat and nwlng position with the values from the position where you have left your map marker
   */
  const handleNWClick = () => {
    dispatch(
      positionSetAction({
        nwlat: selectedPosition[0],
        nwlng: selectedPosition[1],
        selat: selat,
        selng: selng,
      })
    );
  };

  /**
   * Function that runs when the "Set SE" button is clicked
   * Dispatches the positionSetAction from redux
   * sets the selat and nwlng position with the values from the position where you have left your map marker
   */
  const handleSEClick = () => {
    dispatch(
      positionSetAction({
        nwlat: nwlat,
        nwlng: nwlng,
        selat: selectedPosition[0],
        selng: selectedPosition[1],
      })
    );
  };

  /**
   * Function that sets the position of the marker in the position you have chosen
   * Popup i shown when the marker is clicked, and the user is prompted with two buttons
   * which lets the user Set the SE and NW coordinates in CreateMission and CreateGame.
   * Displays two buttons if the user is in the admingamepage, else only show 1.
   */
  const Markers = () => {
    useMapEvents({
      click(e) {
        setSelectedPosition([e.latlng.lat, e.latlng.lng]);
      },
    });

    return selectedPosition ? (
      <Marker
        key={selectedPosition[0]}
        position={selectedPosition}
        icon={greenIcon}
      >
        <Popup>
          <p>{`LAT: ${selectedPosition[0]}`}</p>
          <p>{`LNG: ${selectedPosition[1]}`}</p>
          {location.pathname === "/creategame" ? (
            <div className={styles.posBtns}>
              <button className={styles.posBtn} onClick={handleNWClick}>
                Set NW
              </button>
              <button className={styles.posBtn} onClick={handleSEClick}>
                Set SE
              </button>
            </div>
          ) : (
            <div className={styles.posBtns}>
              <button className={styles.posBtn} onClick={handleNWClick}>
                Set Mission Location
              </button>
            </div>
          )}
        </Popup>
      </Marker>
    ) : null;
  };

  const ChangeView = () => {
    const map = useMap();
    map.setView(selectedPosition);
    return null;
  };

  useEffect(() => {
    setIsLoading(false);
  }, []);

  return (
    <>
      {isLoading ? (
        <Loader height="25vh" width="100%" />
      ) : (
        <div className={styles.container}>
          <MapContainer
            className={styles.Map}
            center={selectedPosition}
            zoom={16}
            style={{ height: props.height, width: props.width }}
          >
            <Markers />

            <ChangeView />
            <TileLayer
              attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
          </MapContainer>
        </div>
      )}
    </>
  );
};

export default AdminMap;
