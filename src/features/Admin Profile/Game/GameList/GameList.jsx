import styles from "./GameList.module.css";
import { useSelector, useDispatch } from "react-redux";
import { useState, useEffect } from "react";
import { gameSetAction } from "../../../../store/actions/gameActions";
import KeycloakService from "../../../../services/KeycloakService";
import { useHistory } from "react-router-dom";
import Loader from "../../../Loader/Loader";

/**
 * List Component for Admin Profile that lists all the games
 */
export const GameList = () => {
  // Game List Variables
  const username = KeycloakService.getUsername();
  const { game } = useSelector((state) => state.gameReducer);
  const { user } = useSelector((state) => state.userReducer);
  const { player } = useSelector((state) => state.playerReducer);
  const [currentgame, setCurrentGame] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const dispatch = useDispatch();
  const history = useHistory();

  /**
   * useEffect that runs on render
   * dispatches the gameSetAction from redux
   * sets IsLoading to false on completion
   */
  useEffect(() => {
    setCurrentGame(game);
    dispatch(gameSetAction());
    setIsLoading(false);
  }, []);

  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <div className={styles.container}>
          <h1 className={styles.header}>Game List</h1>
          <div className={styles.gamesDiv}>
            <ul className={styles.games}>
              {game.map((g) => {
                return (
                  <li key={g.id} className={styles.gameItem}>
                    <p>{g.name}</p>
                    <p> ID: {g.id}</p>
                    <p> Game State: {g.gameState}</p>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      )}
    </>
  );
};
export default GameList;
