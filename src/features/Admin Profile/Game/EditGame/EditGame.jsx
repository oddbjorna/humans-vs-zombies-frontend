import styles from "./EditGame.module.css";
import { useState } from "react";
import APIService from "../../../../services/APIService";
import { useDispatch } from "react-redux";
import { gameSetAction } from "../../../../store/actions/gameActions";

/**
 * Form Component for editing games in Admin Profile
 */
export const EditGame = () => {
  //Edit Game Variables
  const [gameid, setEditGameId] = useState(0);
  const [gamestate, setGameState] = useState("Registration");

  const dispatch = useDispatch();

  //onChange function for the Game id input
  const onEditGameIdChange = (e) => {
    setEditGameId(e.target.value);
  };

  //onChange function for the Game State select
  const onGameStateChange = (e) => {
    setGameState(e.target.value);
  };

  /**
   * onClick function for the submit button
   * if the action is confirmed:
   * Runs the editGame method in the Apiservice with the values given in the form.
   * dispatches the gameSetAction and upadtes the store with the new values.
   */
  const handleEditClick = async (e) => {
    e.preventDefault();
    if (window.confirm("Are you sure you want to edit this game?")) {
      await APIService.editGame(gameid, gamestate);
      dispatch(gameSetAction());
    }
  };

  return (
    <div className={styles.container}>
      <form className={styles.formValues}>
        <div className={styles.inputDiv}>
          <label htmlFor="Game Id">Game ID</label>
          <input
            onChange={onEditGameIdChange}
            placeholder="Enter Game Id"
            type="text"
          ></input>
        </div>
        <div className={styles.inputDiv}>
          <label htmlFor="Game State">Game State</label>
          <select onChange={onGameStateChange}>
            <option className={styles.option} value="Registration">
              Registration
            </option>
            <option className={styles.option} value="In Progress">
              In Progress
            </option>
            <option className={styles.option} value="Complete">
              Complete
            </option>
          </select>
        </div>

        <button
          className={styles.submitBtn}
          type="submit"
          onClick={handleEditClick}
        >
          Set Game State
        </button>
      </form>
    </div>
  );
};
export default EditGame;
