import styles from "./CreateGame.module.css";
import { useState } from "react";
import APIService from "../../../../services/APIService";
import { useDispatch, useSelector } from "react-redux";
import { gameSetAction } from "../../../../store/actions/gameActions";

/**
 * Form Component for Creating Games in the Admin Profile
 */
const CreateGame = () => {
  //Create Game Variables
  const { nwlat, nwlng, selat, selng } = useSelector(
    (state) => state.positionReducer
  );
  const [gamename, setGameName] = useState("");
  const [description, setDescription] = useState("");

  const dispatch = useDispatch();

  //onChange function for Game Name input
  const handleNameChange = (e) => {
    setGameName(e.target.value);
  };

  //onChange function for Game Description textarea
  const handleDescriptionChange = (e) => {
    setDescription(e.target.value);
  };

  /**
   * onClick function when clicking the create game button
   * if the user confirms the action:
   * awaits the Api and runs the postGame method with the values specified in the form
   * dispatches the gameSetAction to update the store with the new game.
   */
  const handleCreateGameClick = async (e) => {
    e.preventDefault();
    if (window.confirm("Are you sure you want to create this game?")) {
      await APIService.postGame(
        gamename,
        description,
        nwlat,
        nwlng,
        selat,
        selng
      );

      dispatch(gameSetAction());
    }
  };

  return (
    <div className={styles.container}>
      <form className={styles.formValues}>
        <div className={styles.name}>
          <label>Game Name</label>
          <input
            className={styles.input}
            onChange={handleNameChange}
            maxLength="15"
            required="required"
            placeholder={`"Cool Game"`}
          ></input>
        </div>
        <div className={styles.name}>
          <label>Description</label>
          <textarea
            className={`${styles.input} ${styles.description}`}
            onChange={handleDescriptionChange}
            required="required"
            placeholder="Game Description..."
          ></textarea>
        </div>
        <div className={styles.inputs}>
          <div className={styles.nw}>
            <div className={styles.labelDiv}>
              <label className={styles.label}>North West Latitude</label>
            </div>

            <input
              readOnly
              className={`${styles.input} ${styles.latlng}`}
              placeholder="1234"
              required="required"
              type="number"
              value={nwlat}
            />
            <div className={styles.labelDiv}>
              <label className={styles.label}>North West Longitude</label>
            </div>

            <input
              readOnly
              className={`${styles.input} ${styles.latlng}`}
              placeholder="1234"
              required="required"
              type="number"
              value={nwlng}
            />
          </div>
          <div className={styles.se}>
            <div className={styles.labelDiv}>
              <label className={styles.label}>South East Latitude</label>
            </div>

            <input
              readOnly
              className={`${styles.input} ${styles.latlng}`}
              placeholder="1234"
              required="required"
              type="number"
              value={selat}
            />
            <div className={styles.labelDiv}>
              <label className={`${styles.label} ${styles.lastLabel}`}>
                South East Longitude
              </label>
            </div>

            <input
              readOnly
              className={`${styles.input} ${styles.latlng}`}
              placeholder="1234"
              required="required"
              type="number"
              value={selng}
            />
          </div>
        </div>
        <div className={styles.btnDiv}>
          <button
            onClick={handleCreateGameClick}
            type="submit"
            className={styles.createBtn}
          >
            Create
          </button>
        </div>
      </form>
    </div>
  );
};
export default CreateGame;
