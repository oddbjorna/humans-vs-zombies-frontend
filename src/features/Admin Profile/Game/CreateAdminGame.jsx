import CreateGame from "./CreateGame/CreateGame";
import EditGame from "./EditGame/EditGame";
import GameList from "./GameList/GameList";
import { SideBar } from "../Sidebar/SideBar";
import styles from "./AdminGame.module.css";
import AdminMap from "../AdminMap/AdminMap";
import withKeycloak from "../../../hoc/withKeycloak";
import $ from "jquery";
import AdminMenu from "../AdminMenu/AdminMenu";
import { useState } from "react";

//Parent Component for the AdminGame Page
const CreateAdminGame = () => {
  //AdminGame Variables
  const [createActive, setCreateActive] = useState(true);
  const [mapActive, setMapActive] = useState(false);

  //onClick functions that displays and hides components
  const handleMapClick = () => {
    if (mapActive) {
      setMapActive(false);
    } else {
      setMapActive(true);
    }
  };
  return (
    <div className={styles.container}>
      <div>
        <h1 className={styles.header}>Create Game</h1>
      </div>
      <div className={styles.createGameElements}>
        {createActive && <CreateGame />}
        {($(window).width() > 600 || mapActive) && (
          <div className={styles.map}>
            <AdminMap
              height={
                $(window).width() <= 600 ? "calc(100vh - 6.5rem)" : "50vh"
              }
              width={$(window).width() <= 600 ? "100vw" : "80vh"}
            />
          </div>
        )}
      </div>

      <AdminMenu handleMapClick={handleMapClick} />
      <SideBar />
    </div>
  );
};
export default CreateAdminGame;
