import styles from "./AdminGame.module.css";
import $ from "jquery";
import { useState } from "react";
import AdminMenu from "../AdminMenu/AdminMenu";
import EditGame from "./EditGame/EditGame";
import GameList from "./GameList/GameList";
import { SideBar } from "../Sidebar/SideBar";

const EditAdminGame = () => {
  const [listActive, setListActive] = useState(false);
  const [editActive, setEditActive] = useState(true);
  const handleListClick = () => {
    if (listActive) {
      setListActive(false);
    } else {
      setListActive(true);
    }
  };
  return (
    <div className={styles.container}>
      <div>
        <h1 className={styles.header}>Edit Game</h1>
      </div>
      <div className={styles.createGameElements}>
        {editActive && <EditGame />}
        {($(window).width() > 600 || listActive) && (
          <div className={styles.gameList}>
            <GameList />
          </div>
        )}
      </div>

      <AdminMenu handleListClick={handleListClick} />
      <SideBar />
    </div>
  );
};

export default EditAdminGame;
