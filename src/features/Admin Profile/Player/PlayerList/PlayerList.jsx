import styles from "./PlayerList.module.css";
import { useSelector, useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import APIService from "../../../../services/APIService";
import { playerSetAction } from "../../../../store/actions/playerActions";

/**
 * List Component for Admin Profile That lists all the games
 */
export const PlayerList = () => {
  //Player List Variables
  const { player } = useSelector((state) => state.playerReducer);
  const [currentplayer, setCurrenPlayer] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const dispatch = useDispatch();

  /**
   * useEffect that runs on render
   * dispatches the playerSetaction
   * sets isLoading to false on completion
   */
  useEffect(() => {
    setCurrenPlayer(player);
    dispatch(playerSetAction());
    setIsLoading(false);
  }, []);

  useEffect(() => {
    if (!isLoading) {
      dispatch(playerSetAction());
    }
  }, []);

  return (
    <>
      {!isLoading && (
        <div className={styles.container}>
          <h1 className={styles.header}>Player List</h1>
          <div className={styles.gamesDiv}>
            <ul className={styles.games}>
              {player.map((p) => {
                return (
                  <li key={p.id} className={styles.gameItem}>
                    <p>{p.username}</p>
                    <p>ID: {p.id}</p>
                    <p>Game Id: {p.gameId}</p>
                    <p>{p.isHuman ? "Human" : "Zombie"}</p>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      )}
    </>
  );
};
export default PlayerList;
