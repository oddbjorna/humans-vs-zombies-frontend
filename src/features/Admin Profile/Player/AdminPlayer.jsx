import EditPlayer from "./EditPlayer/EditPlayer";
import PlayerList from "./PlayerList/PlayerList";
import { SideBar } from "../Sidebar/SideBar";
import styles from "./AdminPlayer.module.css";

/**
 * Parent Component for the AdminPlayer page
 */
export const AdminPlayer = () => {
  return (
    <div className={styles.container}>
      <EditPlayer />
      <PlayerList />
      <SideBar />
    </div>
  );
};
export default AdminPlayer;
