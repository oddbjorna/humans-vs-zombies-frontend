import styles from "./EditPlayer.module.css";
import { useState } from "react";
import APIService from "../../../../services/APIService";
import { useDispatch } from "react-redux";
import { playerSetAction } from "../../../../store/actions/playerActions";

/**
 * Form Component for editing player state in Admin Profile
 */
export const EditPlayer = () => {
  //Edit Player Variables
  const [playerid, setPlayerId] = useState(0);
  const [ishuman, setIshuman] = useState(true);

  const dispatch = useDispatch();

  //Function that converts option value from string to bool
  var str2bool = (value) => {
    if (value && typeof value === "string") {
      if (value.toLowerCase() === "true") return true;
      if (value.toLowerCase() === "false") return false;
    }
    return value;
  };

  //onChange functio for Player Id input
  const onPlayerIdChange = (e) => {
    setPlayerId(e.target.value);
  };
  //onChange function for IsHuman select
  const onIshumanChange = (e) => {
    setIshuman(str2bool(e.target.value));
  };

  /**
   * onClick function for the submit button
   * if the action is confirmed:
   * runs the editPlayer method from the APIservice
   * dispatches the playerSetAction from redux
   */
  const handleEditPlayerSubmitclick = async (e) => {
    e.preventDefault();
    if (window.confirm("Are you sure you want to edit this player?")) {
      await APIService.editPlayer(playerid, ishuman);
      dispatch(playerSetAction());
    }
  };

  return (
    <div className={styles.container}>
      <h1 className={styles.header}>Edit Player</h1>
      <form className={styles.formValues}>
        <div className={styles.inputDiv}>
          <label>Player ID</label>
          <input
            onChange={onPlayerIdChange}
            placeholder="Enter PlayerID"
            type="number"
            name=""
            id=""
          />
          <label>Is Human</label>
          <select onChange={onIshumanChange} name="" id="">
            <option value={true}>True</option>
            <option value={false}>False</option>
          </select>
        </div>

        <button
          type="submit"
          className={styles.submitBtn}
          onClick={handleEditPlayerSubmitclick}
        >
          Submit
        </button>
      </form>
    </div>
  );
};
export default EditPlayer;
