import styles from "./AdminMission.module.css";
import $ from "jquery";
import MissionGameList from "./MissionGameList/MissionGameList";
import EditMission from "./EditMission/EditMission";
import AdminMenu from "../AdminMenu/AdminMenu";
import { SideBar } from "../Sidebar/SideBar";
import { useState } from "react";

const EditAdminMission = () => {
  const [listActive, setListActive] = useState(false);
  const [editActive, setEditActive] = useState(true);

  //onClick Functions that show/hides the different components
  const handleListClick = () => {
    if (listActive) {
      setListActive(false);
    } else {
      setListActive(true);
    }
  };
  return (
    <div className={styles.container}>
      <div>
        <h1 className={styles.header}>Edit Mission</h1>
      </div>
      <div className={styles.createMissionElements}>
        {editActive && <EditMission />}
        {($(window).width() > 600 || listActive) && <MissionGameList />}
      </div>

      <AdminMenu handleListClick={handleListClick} />
      <SideBar />
    </div>
  );
};

export default EditAdminMission;
