import styles from "./EditMission.module.css";
import { useState } from "react";
import APIService from "../../../../services/APIService";
import { useDispatch } from "react-redux";
import { missionSetAction } from "../../../../store/actions/missionActions";

/**
 * Form Component for editing missions in Admin Profile
 */
export const EditMission = () => {
  //Edit Mission Variables
  const [editmissionid, setEditMissionId] = useState(0);
  const [editmissiondescription, setEditMissionDescription] = useState("");

  const dispatch = useDispatch();

  //onChange function for the Mission Id input
  const onEditMissionIdChange = (e) => {
    setEditMissionId(e.target.value);
  };
  //onChange function for the Mission Description textarea
  const onEditMissionDescriptionChange = (e) => {
    setEditMissionDescription(e.target.value);
  };

  /**
   * onClick function for the submit button
   * if the action is confirmed:
   * runs the editMission method in the APIservice
   * dispatches the missionSetAction from redux
   */
  const onEditMissionSubmit = async (e) => {
    e.preventDefault();
    if (window.confirm("Are you sure you want to edit this mission?")) {
      await APIService.editMission(editmissionid, editmissiondescription);
      dispatch(missionSetAction());
    }
  };

  return (
    <div className={styles.container}>
      <h1 className={styles.header}>Edit Mission</h1>
      <form className={styles.formValues}>
        <div className={styles.inputDiv}>
          <label>Mission ID</label>
          <input
            onChange={onEditMissionIdChange}
            placeholder="Enter Mission Id"
            type="number"
          ></input>
          <label>Description</label>
          <textarea
            onChange={onEditMissionDescriptionChange}
            required="required"
            className={styles.missionDescription}
            rows="4"
            cols="25"
            name="comment"
            form="missionform"
            placeholder="Enter description here"
          ></textarea>
        </div>

        <button
          className={styles.submitBtn}
          type="submit"
          onClick={onEditMissionSubmit}
        >
          Submit
        </button>
      </form>
    </div>
  );
};
export default EditMission;
