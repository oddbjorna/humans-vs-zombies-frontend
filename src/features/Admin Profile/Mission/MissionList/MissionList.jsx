import styles from "./MissionList.module.css";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { missionSetAction } from "../../../../store/actions/missionActions";
import Loader from "../../../Loader/Loader";
export const MissionList = () => {
  //Mission list Varibles
  const { mission } = useSelector((state) => state.missionReducer);
  const [currentmission, setCurrentMission] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const dispatch = useDispatch();

  //Dispatches the missionSetAction from redux store on render to update missionlist
  useEffect(() => {
    setCurrentMission(mission);
    dispatch(missionSetAction());
    setIsLoading(false);
  }, []);

  useEffect(() => {
    if (!isLoading) {
      dispatch(missionSetAction());
    }
  }, []);

  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <div className={styles.container}>
          <h1 className={styles.header}>Mission List</h1>
          <div className={styles.gamesDiv}>
            <ul className={styles.games}>
              {mission.map((m) => {
                return (
                  <li key={m.id} className={styles.gameItem}>
                    <h3>{m.name}</h3>
                    <p>Mission ID: {m.id}</p>
                    <p>Description: {m.description}</p>
                    <p>Game ID: {m.gameId}</p>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      )}
    </>
  );
};
export default MissionList;
