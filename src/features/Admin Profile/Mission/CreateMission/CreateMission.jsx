import styles from "./CreateMission.module.css";
import { useEffect, useState } from "react";
import APIService from "../../../../services/APIService";
import { useSelector, useDispatch } from "react-redux";
import { missionSetAction } from "../../../../store/actions/missionActions";

/**
 * Form Component for creating missions in the Admin Profile
 */
export const CreateMission = () => {
  //Create Mission Variables
  const [missionname, setMissionName] = useState("");
  const [description, setDescription] = useState("");
  const [humanvisible, setHumanVisible] = useState(true);
  const [zombievisible, setZombieVisible] = useState(true);
  const [missiongameId, setMissionGameId] = useState("");

  const { connection } = useSelector((state) => state.connectionReducer);

  const { nwlat, nwlng } = useSelector((state) => state.positionReducer);
  const dispatch = useDispatch();

  //Function that converts the string value from the select option to a bool based on chosen option
  var str2bool = (value) => {
    if (value && typeof value === "string") {
      if (value.toLowerCase() === "true") return true;
      if (value.toLowerCase() === "false") return false;
    }
    return value;
  };

  //onChange function for Mission name input
  const onMissionNameChange = (e) => {
    setMissionName(e.target.value);
  };
  //onChange functio for the Misison Description textarea
  const onDescriptionChange = (e) => {
    setDescription(e.target.value);
  };
  //onChange function for the Human Visible select, converts the value to bool
  const onHumanVisiblecChange = (e) => {
    setHumanVisible(str2bool(e.target.value));
  };
  //onChange function for the Zombie Visible select, converts the value to bool
  const onZombieVisiblecChange = (e) => {
    setZombieVisible(str2bool(e.target.value));
  };
  //onChange function for the Mission Id input
  const onMissionGameIdChange = (e) => {
    setMissionGameId(e.target.value);
  };

  /**
   * onClick function for the create mission button
   * if the user confirms the action:
   * awaits the API and runs the postMission method with form values as parameters
   * dispatches the missionSetAction from redux store
   * sends the mission using websockets to update the markes on the map for all players
   */
  const handleCreateMissionClick = async (e) => {
    e.preventDefault();
    if (window.confirm("Are you sure you want to create a mission?")) {
      await APIService.postMission(
        missionname,
        description,
        nwlat,
        nwlng,
        humanvisible,
        zombievisible,
        missiongameId
      );
      dispatch(missionSetAction());
      connection.send("SendMission", { marker: "missionMarker" });
    }
  };

  return (
    <div className={styles.container}>
      <form className={styles.formValues}>
        <div className={styles.name}>
          <label>Mission Name</label>
          <input
            className={styles.input}
            onChange={onMissionNameChange}
            required="required"
            placeholder="Enter Mission Name"
            type="text"
          />
        </div>

        <div className={styles.name}>
          <label>Description</label>
          <textarea
            onChange={onDescriptionChange}
            required="required"
            className={`${styles.input} ${styles.description}`}
            name="comment"
            form="missionform"
            placeholder="Enter description here"
          ></textarea>
        </div>
        <div className={styles.inputs}>
          <div className={styles.nw}>
            <label className={styles.label}>Latitude</label>
            <input
              className={`${styles.input} ${styles.latlng}`}
              readOnly
              value={nwlat}
              type="number"
              required="required"
              placeholder="12313"
            ></input>
            <label className={styles.label}>Longitude</label>
            <input
              className={`${styles.input} ${styles.latlng}`}
              readOnly
              value={nwlng}
              type="number"
              required="required"
              placeholder="12313"
            ></input>
          </div>
          <div className={styles.se}>
            <label className={styles.label}>Is Visible for Humans</label>
            <select
              onChange={onHumanVisiblecChange}
              className={`${styles.input} ${styles.latlng}`}
            >
              <option value={true}>True</option>
              <option value={false}>False</option>
            </select>
            <label className={styles.label}>Is Visible for Zombies</label>
            <select
              onChange={onZombieVisiblecChange}
              className={`${styles.input} ${styles.latlng}`}
            >
              <option value={true}>True</option>
              <option value={false}>False</option>
            </select>
            <label className={styles.label}>Game ID</label>
            <input
              className={`${styles.input} ${styles.latlng}`}
              onChange={onMissionGameIdChange}
              required="required"
              placeholder="Enter GameID"
              type="number"
            />
          </div>
        </div>
        <div className={styles.btnDiv}>
          <button
            type="submit"
            onClick={handleCreateMissionClick}
            className={styles.createBtn}
          >
            Create
          </button>
        </div>
      </form>
    </div>
  );
};
export default CreateMission;
