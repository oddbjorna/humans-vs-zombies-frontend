import GameList from "../../Game/GameList/GameList";
import MissionList from "../MissionList/MissionList";
import styles from "./MissionGameList.module.css";
/**
 * Parent component that contains the Game- and Missionlist in the AdminMission Component
 */
const MissionGameList = () => {
  return (
    <div className={styles.container}>
      <GameList />
      <MissionList />
    </div>
  );
};

export default MissionGameList;
