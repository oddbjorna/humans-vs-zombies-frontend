import CreateMission from "./CreateMission/CreateMission";
import EditMission from "./EditMission/EditMission";
import MissionList from "./MissionList/MissionList";
import { SideBar } from "../Sidebar/SideBar";
import styles from "./AdminMission.module.css";
import AdminMap from "../AdminMap/AdminMap";
import $ from "jquery";
import Map from "../../Map/Map";
import MissionGameList from "./MissionGameList/MissionGameList";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { connectionSetAction } from "../../../store/actions/connectionActions";
import AdminMenu from "../AdminMenu/AdminMenu";
import { missionSetAction } from "../../../store/actions/missionActions";

/**
 * Parent Component for the AdminMission Page
 */
export const CreateAdminMission = () => {
  const user = JSON.parse(localStorage.getItem("user"));
  const [isLoading, setIsLoading] = useState(true);
  const { connection } = useSelector((state) => state.connectionReducer);
  const dispatch = useDispatch();

  const [createActive, setCreateActive] = useState(true);
  const [mapActive, setMapActive] = useState(false);
  const [listActive, setListActive] = useState(false);
  const [editActive, setEditActive] = useState(false);

  //onClick Functions that show/hides the different components
  const handleMapClick = () => {
    if (mapActive) {
      setMapActive(false);
    } else {
      setMapActive(true);
      setListActive(false);
      setEditActive(false);
    }
  };
  const handleListClick = () => {
    if (listActive) {
      setListActive(false);
    } else {
      setListActive(true);
      setEditActive(false);
      setMapActive(false);
    }
  };
  const handleEditClick = () => {
    if (editActive) {
      setEditActive(false);
    } else {
      setEditActive(true);
      setMapActive(false);
      setListActive(false);
    }
  };
  //Dispatches the connectionSetaction and missionSetAction on render, sets isLoading to false on completion
  useEffect(() => {
    dispatch(connectionSetAction());
    dispatch(missionSetAction());
    setIsLoading(false);
  }, []);

  //Initializes connection to websockets
  useEffect(() => {
    if (!isLoading) {
      if (connection) {
        connection
          .start()
          .then((result) => {
            console.log("Connected to gameHub!");
          })
          .catch((e) => console.log("Connection failed", e));
      }
      return () => {
        connection.stop().then(() => console.log("Stopped"));
      };
    }
  }, [connection]);

  return (
    <div className={styles.container}>
      <div>
        <h1 className={styles.header}>Create Mission</h1>
      </div>
      <div className={styles.createMissionElements}>
        {createActive && <CreateMission />}
        {($(window).width() > 600 || mapActive) && (
          <div className={styles.map}>
            {user ? (
              <Map
                height={
                  $(window).width() <= 600 ? "calc(100vh - 6.5rem)" : "40vh"
                }
                width={$(window).width() <= 600 ? "100vw" : "80vh"}
              />
            ) : (
              <AdminMap height="40vh" />
            )}
          </div>
        )}
      </div>

      <AdminMenu
        handleListClick={handleListClick}
        handleMapClick={handleMapClick}
        handleEditClick={handleEditClick}
      />
      <SideBar />
    </div>
  );
};
export default CreateAdminMission;
