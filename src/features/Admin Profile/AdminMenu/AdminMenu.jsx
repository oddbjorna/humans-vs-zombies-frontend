import styles from "./AdminMenu.module.css";
import KeycloakService from "../../../services/KeycloakService";
import ExitToAppIcon from "@mui/icons-material/ExitToApp";
import APIService from "../../../services/APIService";
import { useState } from "react";
import { useHistory, useLocation } from "react-router-dom";
import AddIcon from "@mui/icons-material/Add";
import PowerSettingsNewIcon from "@mui/icons-material/PowerSettingsNew";
import { useDispatch } from "react-redux";
import { sessionClearAction } from "../../../store/actions/sessionActions";
import MapIcon from "@mui/icons-material/Map";
import FormatListBulletedIcon from "@mui/icons-material/FormatListBulleted";
import EditIcon from "@mui/icons-material/Edit";

/**
 * Menu component for mobile view in admin profile
 */
const AdminMenu = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();
  const [trigger, setTrigger] = useState(false);
  const user = JSON.parse(localStorage.getItem("user"));

  /**
   * Onclick function that logs the user out
   */
  const handleLogoutClick = () => {
    if (window.confirm("Are you sure?")) {
      handleLeaveGameClick();
      KeycloakService.doLogout();
    }
  };

  /**
   * Onlick function that displays the map
   */
  const handleMapClick = () => {
    props.handleMapClick();
    handleTriggerClick();
  };

  /**
   * onClick function that displays Lists of Players,Missions,Games
   */
  const handleListClick = () => {
    props.handleListClick();
    handleTriggerClick();
  };

  /**
   * onClick function that opens the Edit page for Missions,Game and Players
   */
  const handleEditClick = () => {
    props.handleEditClick();
    handleTriggerClick();
  };

  /**
   * Onclick function that exectues the handleLeaveGame function.
   */
  const handleLeaveClick = () => {
    if (user) {
      if (
        window.confirm(
          "Are you sure you want to leave the game? Your progress will be lost"
        )
      ) {
        handleLeaveGameClick();
      }
    }
  };

  /**
   * onClick function to Show/hide the menu.
   */
  const handleTriggerClick = (e) => {
    if (trigger) {
      setTrigger(false);
    } else {
      setTrigger(true);
    }
  };

  /**
   * Logout function that removes player and squadmember from database
   * clears localstorage and session
   * redirects the user to the dashboard
   */
  const handleLeaveGameClick = async () => {
    await APIService.deleteSquadMember(user.playerId);
    await APIService.deletePlayer(user.playerId);

    localStorage.clear();
    dispatch(sessionClearAction());
    history.push("/dashboard");
  };

  return (
    <menu className={trigger ? styles.open : ""}>
      <span onClick={handleLogoutClick} className={styles.action}>
        <i>
          <PowerSettingsNewIcon />
        </i>
      </span>
      <span onClick={handleLeaveClick} className={styles.action}>
        <i>
          <ExitToAppIcon />
        </i>
      </span>
      {(location.pathname === "/creategame" ||
        location.pathname === "/createmission") && (
        <span onClick={handleMapClick} className={styles.action}>
          <i>
            <MapIcon />
          </i>
        </span>
      )}
      {(location.pathname === "/editgame" ||
        location.pathname === "/editmission") && (
        <span onClick={handleListClick} className={styles.action}>
          <i>
            <FormatListBulletedIcon />
          </i>
        </span>
      )}

      <span className={styles.trigger} onClick={handleTriggerClick}>
        <i>
          <AddIcon />
        </i>
      </span>
    </menu>
  );
};
export default AdminMenu;
