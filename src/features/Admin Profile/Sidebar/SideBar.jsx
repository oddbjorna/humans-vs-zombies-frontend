import { useHistory } from "react-router";
import styles from "./SideBar.module.css";
import MenuIcon from "@mui/icons-material/Menu";
import CloseIcon from "@mui/icons-material/Close";
import { useState } from "react";
import CreateIcon from "@mui/icons-material/Create";
import AddIcon from "@mui/icons-material/Add";

/**
 * Sidebar Component for the AdminProfile
 */
export const SideBar = () => {
  const [active, setActive] = useState(false);
  const [gameFeatActive, setGameFeatActive] = useState(false);
  const [missionFeatActive, setMissionFeatActive] = useState(false);

  const handleGameFeats = () => {
    if (gameFeatActive) {
      setGameFeatActive(false);
    } else {
      setGameFeatActive(true);
    }
  };

  const handleMissionFeats = () => {
    if (missionFeatActive) {
      setMissionFeatActive(false);
    } else {
      setMissionFeatActive(true);
    }
  };

  //Show/Hide function for the sidebar
  const handleActive = () => {
    if (active) {
      setActive(false);
    } else {
      setActive(true);
    }
  };

  const history = useHistory();

  //onClick functions that redirects the user to the different AdminProfile Pages
  const handleGameClick = () => {
    history.push("/creategame");
  };

  const handleEditGameClick = () => {
    history.push("/editgame");
  };

  const handleMissionClick = () => {
    history.push("/createmission");
  };

  const handleEditMissionClick = () => {
    history.push("/editmission");
  };

  const handlePlayerClick = () => {
    history.push("/adminplayer");
  };

  const handleAdminClick = () => {
    history.push("/admin");
  };

  return (
    <div className={styles.container}>
      <div
        className={active ? `${styles.sidebar} ${styles.open}` : styles.sidebar}
      >
        <div onClick={handleActive} className={styles.icon}>
          <CloseIcon className={styles.innerIcon} />
        </div>

        <header>Features</header>
        <ul>
          <li className={styles.sideBarItem}>
            <button onClick={handleAdminClick}>Admin Home</button>
          </li>
          <li className={styles.sideBarItem}>
            <button onClick={handleGameFeats}>Game</button>
          </li>
          {gameFeatActive && (
            <ul className={styles.gameFeats}>
              <li className={styles.gameFeatsItem}>
                <button className={styles.featButton} onClick={handleGameClick}>
                  <AddIcon className={styles.listIcon} />
                  Create
                </button>
              </li>
              <li className={styles.gameFeatsItem}>
                <button
                  className={styles.featButton}
                  onClick={handleEditGameClick}
                >
                  <CreateIcon className={styles.listIcon} />
                  Edit
                </button>
              </li>
            </ul>
          )}
          <li className={styles.sideBarItem}>
            <button onClick={handleMissionFeats}>Mission</button>
          </li>
          {missionFeatActive && (
            <ul className={styles.gameFeats}>
              <li className={styles.gameFeatsItem}>
                <button
                  className={styles.featButton}
                  onClick={handleMissionClick}
                >
                  <AddIcon className={styles.listIcon} />
                  Create
                </button>
              </li>
              <li className={styles.gameFeatsItem}>
                <button
                  className={styles.featButton}
                  onClick={handleEditMissionClick}
                >
                  <CreateIcon className={styles.listIcon} />
                  Edit
                </button>
              </li>
            </ul>
          )}
          <li className={styles.sideBarItem}>
            <button onClick={handlePlayerClick}>Player</button>
          </li>
        </ul>
      </div>

      {!active && (
        <div onClick={handleActive} className={styles.icon}>
          <MenuIcon className={styles.innerIcon} />
        </div>
      )}
    </div>
  );
};
