import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import KeycloakService from "../../services/KeycloakService";
import styles from "./AdminProfile.module.css";
import { useSelector, useDispatch } from "react-redux";
import GameCard from "../Dashboard/GameCard";
import Game from "../Game/Game.jsx";
import { SideBar } from "./Sidebar/SideBar";
import { missionSetAction } from "../../store/actions/missionActions";
import withKeycloak from "../../hoc/withKeycloak";
import { gameSetAction } from "../../store/actions/gameActions";
import AdminMap from "./AdminMap/AdminMap";
import Chat from "../Chat/Chat";
import Map from "../Map/Map";
import $ from "jquery";
import { connectionSetAction } from "../../store/actions/connectionActions";
import { playerSetAction } from "../../store/actions/playerActions";
import { squadSetAction } from "../../store/actions/squadActions";
import APIService from "../../services/APIService";
import {
  sessionClearAction,
  sessionSetAction,
} from "../../store/actions/sessionActions";
import { userSetAction } from "../../store/actions/userActions";
import { chatSetAction } from "../../store/actions/chatActions";
import AddIcon from "@mui/icons-material/Add";

/**
 * Landing Page for the Admin Profile
 */
const AdminProfile = () => {
  const role = KeycloakService.hasRole(["admin"]);
  const dispatch = useDispatch();
  const history = useHistory();
  const [isLoading, setIsLoading] = useState(true);
  const [inGame, setInGame] = useState(false);
  const [active, setActive] = useState(false);
  const [session, setSession] = useState(false);

  const { game } = useSelector((state) => state.gameReducer);
  const { connection } = useSelector((state) => state.connectionReducer);
  const { user } = useSelector((state) => state.userReducer);
  const { player } = useSelector((state) => state.playerReducer);

  const sessionUser = JSON.parse(localStorage.getItem("user"));

  const username = KeycloakService.getUsername();

  const handleLeaveGameClick = async () => {
    const localUser = JSON.parse(localStorage.getItem("user"));
    if (localUser) {
      await APIService.deleteSquadMember(localUser.playerId);
      await APIService.deletePlayer(localUser.playerId);
      //localStorage.clear();
      //dispatch(sessionClearAction());
      setSession(true);
    }
  };

  const handleActive = () => {
    if (active) {
      setActive(false);
    } else {
      setActive(true);
    }
  };

  const makePlayer = async (gameid) => {
    setIsLoading(true);
    setInGame(false);
    try {
      await handleLeaveGameClick();
      const players = await APIService.getPlayers();
      const playerToBe = user.filter((u) => u.username === username)[0];
      const playerExists = players.filter((p) => p.userId === playerToBe.id);
      if (!playerExists.length) {
        APIService.postPlayer(playerToBe.id, gameid);
      }
    } catch (e) {
      console.error(e);
    }
  };

  useEffect(() => {
    if (!role) {
      history.push("/dashboard");
    }
    dispatch(gameSetAction());
    dispatch(missionSetAction());
    dispatch(connectionSetAction());

    setIsLoading(false);
  }, []);

  useEffect(() => {
    if (!isLoading) {
      dispatch(userSetAction());
      dispatch(playerSetAction());
      dispatch(gameSetAction());
      dispatch(squadSetAction());
    }

    setInGame(true);
  }, [isLoading]);

  useEffect(() => {
    setSession(true);
  }, [player]);

  useEffect(() => {
    if (session) {
      setTimeout(() => dispatch(sessionSetAction()), 500);
      setIsLoading(false);
    }
    setSession(false);
  }, [session]);

  useEffect(() => {
    if (!isLoading) {
      if (connection) {
        connection
          .start()
          .then((result) => {
            console.log("Connected to gameHub!");

            connection.on("ReceiveMarker", (message) => {
              dispatch(squadSetAction());
            });
            connection.on("ReceiveKill", (victim) => {
              dispatch(playerSetAction());
              //handleKillReceived(victim);
            });
            connection.on("ReceiveMission", (mission) => {
              dispatch(missionSetAction());
            });
          })
          .catch((e) => console.log("Connection failed", e));
      }
      return () => {
        if (connection) {
          connection.stop().then(() => {
            console.log("stopped");
          });
        }
      };
    }
  }, [connection]);

  return (
    <main>
      <div className={styles.container}>
        <div className={`${styles.column} ${styles.gamesDiv}`}>
          <ul className={styles.games}>
            {game.map((g) => {
              return g.gameState === "Registration" ? (
                <li key={g.id} className={styles.listItem}>
                  <GameCard game={g} handleJoinClick={makePlayer} />
                </li>
              ) : g.gameState === "Complete" ? (
                ""
              ) : (
                <div className={styles.disableCard}>
                  <li key={g.id} className={styles.listItem}>
                    <GameCard game={g} handleJoinClick={makePlayer} />
                  </li>
                </div>
              );
            })}
          </ul>
        </div>
        {(active || $(window).width() > 600) && (
          <div className={`${styles.column} ${styles.chatMap}`}>
            <div className={styles.map}>
              {role && JSON.parse(localStorage.getItem("user")) ? (
                <Map
                  height={$(window).width() <= 600 ? "30vh" : "35vh"}
                  width={$(window).width() <= 600 ? "100vw" : "80vh"}
                />
              ) : (
                <AdminMap />
              )}
            </div>
            <div className={styles.chat}>
              {user ? (
                <Chat
                  //inSquad={inSquad}
                  connection={connection}
                  height={$(window).width() <= 600 ? "50vh" : "50vh"}
                  width={$(window).width() <= 600 ? "100vw" : "80vh"}
                  inGame={inGame}
                />
              ) : (
                <div></div>
              )}
            </div>
          </div>
        )}

        <button onClick={handleActive} className={styles.exit}>
          <AddIcon />
        </button>
        <SideBar />
      </div>
    </main>
  );
};
export default withKeycloak(AdminProfile);
