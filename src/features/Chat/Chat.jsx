import styles from "./Chat.module.css";
import SendIcon from "@mui/icons-material/Send";
import { useState, useEffect, useRef } from "react";
import APIService from "../../services/APIService";
import { useDispatch, useSelector } from "react-redux";
import { chatSetAction } from "../../store/actions/chatActions";
import moment from "moment";

const Chat = (props) => {
  const [input, setInput] = useState("");
  const [squad, setSquad] = useState(false);
  const [faction, setFaction] = useState(false);
  const chatRef = useRef(null);

  const { session } = useSelector((state) => state.sessionReducer);

  const { globalChat, squadChat, factionChat } = useSelector(
    (state) => state.chatReducer
  );

  const dispatch = useDispatch();

  const handleInputChange = (e) => {
    setInput(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (squad) {
      postSquadChat(input);
    } else if (faction) {
      postFactionChat(input);
    } else {
      postGlobalChat(input);
    }
    document.getElementById("form").reset();
  };

  const handleGlobalButtonClick = () => {
    setSquad(false);
    setFaction(false);
  };
  const handleSquadButtonClick = () => {
    getChat();
    const localSessionUser = JSON.parse(localStorage.getItem("user"));
    if (localSessionUser.squadId) {
      setFaction(false);
      setSquad(true);
    }
  };
  const handleFactionButtonClick = () => {
    getChat();
    const localSessionUser = JSON.parse(localStorage.getItem("user"));
    if (localSessionUser.human || localSessionUser.zombie) {
      setSquad(false);
      setFaction(true);
    }
  };

  const getChat = async () => {
    dispatch(chatSetAction());
  };

  const postGlobalChat = async (message) => {
    if (props.connection.connectionStarted) {
      await APIService.postGlobalChat(message);
      await props.connection.send("SendMessage", { message });
      dispatch(chatSetAction());
    }
  };

  const postSquadChat = async (message) => {
    if (props.connection.connectionStarted) {
      await APIService.postSquadChat(message);
      await props.connection.send("SendMessage", { message });
      dispatch(chatSetAction());
    }
  };

  const postFactionChat = async (message) => {
    if (props.connection.connectionStarted) {
      await APIService.postFactionChat(message);
      await props.connection.send("SendMessage", { message });
      dispatch(chatSetAction());
      chatRef.current.scrollIntoView({
        behaviour: "smooth",
        block: "nearest",
        inline: "start",
      });
    }
  };

  useEffect(() => {
    if (props.inGame) {
      dispatch(chatSetAction());
    }
  }, [props.inGame]);

  useEffect(() => {
    getChat();
    if (!props.inSquad) {
      setSquad(false);
    }
  }, [props.inSquad]);

  useEffect(() => {
    if (props.connection) {
      props.connection.on("ReceiveMessage", (message) => {
        getChat();
      });
    }
  }, [props.connection]);
  return (
    <>
      {!props.inGame || !session ? (
        <div style={{ height: props.height, width: props.width }}>
          <h1>Join a game to see chat</h1>
        </div>
      ) : (
        <div
          className={styles.outerContainer}
          style={{ height: props.height, width: props.width }}
        >
          <h4 className={styles.header}>
            Game Chat:{" "}
            {JSON.parse(localStorage.getItem("user")).gameName +
              " ( " +
              JSON.parse(localStorage.getItem("user")).gameId +
              " )"}
          </h4>
          <ul className={styles.chatContainer}>
            {(props.inSquad && squad
              ? squadChat
              : faction
              ? factionChat
              : globalChat
            ).map((chat) => {
              return (
                <li
                  key={chat.id}
                  className={
                    chat.userId ===
                    JSON.parse(localStorage.getItem("user")).userId
                      ? styles.right
                      : styles.left
                  }
                >
                  <div className={styles.messageContainer}>
                    <p
                      className={
                        chat.userId ===
                        JSON.parse(localStorage.getItem("user")).userId
                          ? `${styles.right} ${styles.player}`
                          : `${styles.left} ${styles.player}`
                      }
                    >
                      {chat.username} {moment(chat.chatTime).format("HH:mm")}
                    </p>
                    <p
                      className={
                        chat.userId ===
                        JSON.parse(localStorage.getItem("user")).userId
                          ? styles.user
                          : styles.other
                      }
                    >
                      {chat.message}
                    </p>
                  </div>
                </li>
              );
            })}
            <span ref={chatRef}></span>
          </ul>
          <form id="form" className={styles.inputContainer}>
            <input
              autoComplete="off"
              type="text"
              onChange={handleInputChange}
              className={styles.input}
              id="input"
              placeholder={`Send message to ${
                squad ? "#squad" : faction ? "#faction" : "#global"
              }`}
            />
            <button
              className={styles.submit}
              type="submit"
              onClick={handleSubmit}
            >
              <SendIcon />
            </button>
          </form>
          <ul className={styles.tabs}>
            <li
              onClick={handleGlobalButtonClick}
              className={styles.tab}
              style={
                !squad && !faction
                  ? { background: "#80e200", color: "#000000" }
                  : {}
              }
            >
              Global
            </li>
            <li
              onClick={handleSquadButtonClick}
              className={styles.tab}
              style={squad ? { background: "#80e200", color: "#000000" } : {}}
            >
              Squad
            </li>
            <li
              onClick={handleFactionButtonClick}
              className={styles.tab}
              style={faction ? { background: "#80e200", color: "#000000" } : {}}
            >
              Faction
            </li>
          </ul>
        </div>
      )}
    </>
  );
};
export default Chat;
