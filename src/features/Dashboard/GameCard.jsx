import styles from "./GameCard.module.css";
import { useSelector } from "react-redux";
import { useState } from "react";
import DashboardMap from "../Map/DashboardMap";

/**
 * GameCard Component that is displayed in the Dashboard Component.
 * displays how many players that are in the game, game description etc.
 */
const GameCard = (props) => {
  //GameCard Variables
  const [extended, setExtended] = useState(false);
  const { player } = useSelector((state) => state.playerReducer);
  const playerArray = player.filter((p) => p.gameId === props.game.id);
  const { squad } = useSelector((state) => state.squadReducer);
  const squadArray = squad.filter((s) => s.gameId === props.game.id);

  /**
   * Function to Show/Hide the extended version of the GameCard
   * Contains additional information about the game.
   */
  const handleCardClick = () => {
    if (extended) {
      setExtended(false);
    } else {
      setExtended(true);
    }
  };

  return (
    <div className={styles.cardContainer}>
      <div className={styles.header} onClick={handleCardClick}>
        <div>
          <h1 className={styles.title}>{props.game.name}</h1>
          <h3>Game State: {props.game.gameState}</h3>
        </div>
        <div className={styles.zombies}>
          <img
            className={styles.zombie}
            src={`/zombies/${Math.floor(Math.random() * (5 - 1 + 1) + 1)}.png`}
            alt="zombie"
          />
          <img
            className={styles.zombie}
            src={`/zombies/${Math.floor(Math.random() * (5 - 1 + 1) + 1)}.png`}
            alt="zombie"
          />
        </div>
        <h3 className={styles.players}>Players:</h3>
        <h2 className={styles.players}>{playerArray.length} / 50</h2>
      </div>

      {extended && (
        <div>
          <div className={styles.gameDescriptionContainer}>
            <div className={styles.gameDescriptionColumn}>
              <div className={styles.gameDescription}>
                <div>
                  <h3 className={styles.descriptionContainer}>Description:</h3>
                  <p className={styles.description}>{props.game.description}</p>
                </div>

                <div>
                  <div className={styles.squadList}>
                    <h3>Squads:</h3>
                    <ul className={styles.squadUl}>
                      {squadArray.map((s) => {
                        return (
                          <li key={s.id}>
                            <div>
                              <p className={styles.squadListItems}>{s.name}</p>
                            </div>
                          </li>
                        );
                      })}
                    </ul>
                  </div>
                </div>
              </div>
              <div className={styles.buttonDiv}>
                <button
                  className={styles.button}
                  onClick={() => {
                    if (playerArray.length >= 50) {
                      alert("This game is full!");
                    } else {
                      props.handleJoinClick(props.game.id);
                    }
                  }}
                >
                  Join Game
                </button>
              </div>
            </div>
            <div className={styles.mapContainer}>
              <DashboardMap game={props.game} />
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default GameCard;
