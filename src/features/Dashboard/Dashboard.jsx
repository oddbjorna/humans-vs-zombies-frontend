import withKeycloak from "../../hoc/withKeycloak";
import styles from "./Dashboard.module.css";
import KeycloakService from "../../services/KeycloakService";
import { useEffect, useState } from "react";
import APIService from "../../services/APIService";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { playerSetAction } from "../../store/actions/playerActions";
import { userSetAction } from "../../store/actions/userActions";
import { gameSetAction } from "../../store/actions/gameActions";
import { sessionSetAction } from "../../store/actions/sessionActions";
import { squadSetAction } from "../../store/actions/squadActions";
import CloseIcon from "@mui/icons-material/Close";
import GameCard from "./GameCard";
import Menu from "../Navbar/Menu";
import Loader from "../Loader/Loader";

/**
 * Dashboard Component that is presented to the user after they have logged in with KeyCloak
 */
const Dashboard = () => {
  //Dashboard Variables
  const username = KeycloakService.getUsername();
  const { game } = useSelector((state) => state.gameReducer);
  const { user } = useSelector((state) => state.userReducer);
  const { player } = useSelector((state) => state.playerReducer);
  const [isLoading, setIsLoading] = useState(true);

  const [rules, setRules] = useState(false);

  const history = useHistory();

  const dispatch = useDispatch();

  //Function that runs the postUser method from the APIService with the logged in user's Username as parameter, reloads the page.
  const postUser = async () => {
    await APIService.postUser(username);
    window.location.reload();
  };

  //Function To Show/Hide the game rules.
  const handleRulesClick = () => {
    if (rules) {
      setRules(false);
    } else {
      setRules(true);
    }
  };

  /**
   * Function that runs when a user tries to join a game
   * Takes the gameId as parameter
   * Checks if the user allready exists, if it does not, the postplayer method is exectued
   * checks if the user is in session, if it does not, the sessionSetAction is dispatched
   * sets a timeout of 500ms and redirects the user to the game page
   */
  const makePlayer = async (gameid) => {
    try {
      const playerToBe = user.filter((u) => u.username === username)[0];
      const playerExists = player.filter((p) => p.userId === playerToBe.id);
      if (!playerExists.length) {
        await APIService.postPlayer(playerToBe.id, gameid);
      }
      const sessionUser = JSON.parse(localStorage.getItem("user"));
      if (sessionUser === null || !sessionUser.squadId) {
        dispatch(sessionSetAction());
      }

      setTimeout(() => history.push("/game"), 500);
    } catch (e) {
      console.error(e);
    }
  };

  /**
   * useEffect that checks if the user exists in database and localstorage
   * creates the user if it does not.
   */
  useEffect(() => {
    const getUsers = async () => {
      const apiUsers = await APIService.getUsers();
      const names = apiUsers.map((u) => u.username);
      if (!names.includes(username)) {
        postUser();
      }
    };
    getUsers();

    const user = JSON.parse(localStorage.getItem("user"));
    if (user === null || !user.squadId) {
      dispatch(sessionSetAction());
    }
    setIsLoading(false);
  }, []);

  //Dispatches severeal SetActions
  useEffect(() => {
    if (!isLoading) {
      dispatch(userSetAction());
      dispatch(playerSetAction());
      dispatch(gameSetAction());
      dispatch(squadSetAction());
    }
  }, [isLoading]);
  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <main className={styles.container}>
          <div className={styles.header}>
            <h1>Join your friends ingame!</h1>
            <div className={styles.gameRules}>
              <h2 className={styles.rulesTitle}>Game rules:</h2>

              <p className={styles.rulesParagraph}>
                1. The game begins with one or more "Original Zombies" (OZ) or
                patient zero. The purpose of the OZ is to infect human players
                by tagging them. Once tagged, a human becomes a zombie for the
                remainder of the game.
              </p>

              <p>
                2. Human players are able to defend themselves against the
                zombie horde using Nerf weapons and clean, rolled-up socks which
                may be thrown to stun an unsuspecting zombie.
              </p>
            </div>
          </div>
          <div className={styles.column}>
            <ul className={styles.games}>
              <menu>
                <span onClick={handleRulesClick} className={styles.rules}>
                  <img
                    alt="paperscroll"
                    className={styles.rulesIcon}
                    src="/paperscroll.svg"
                  />
                </span>
              </menu>
              {rules && (
                <div className={styles.rulesContainer}>
                  <div className={styles.secondRulesContainer}>
                    <button className={styles.close} onClick={handleRulesClick}>
                      <CloseIcon />
                    </button>
                    <div className={styles.gameRules}>
                      <h2 className={styles.rulesTitle}>Game rules:</h2>

                      <p className={styles.rulesParagraph}>
                        1. The game begins with one or more "Original Zombies"
                        (OZ) or patient zero. The purpose of the OZ is to infect
                        human players by tagging them. Once tagged, a human
                        becomes a zombie for the remainder of the game.
                      </p>

                      <p>
                        2. Human players are able to defend themselves against
                        the zombie horde using Nerf weapons and clean, rolled-up
                        socks which may be thrown to stun an unsuspecting
                        zombie.
                      </p>
                    </div>
                  </div>
                </div>
              )}
              {game.map((g) => {
                return g.gameState === "Registration" ? (
                  <li key={g.id} className={styles.listItem}>
                    <GameCard game={g} handleJoinClick={makePlayer} />
                  </li>
                ) : g.gameState === "Complete" ? (
                  ""
                ) : (
                  <div className={styles.disableCard}>
                    <li key={g.id} className={styles.listItem}>
                      <GameCard game={g} handleJoinClick={makePlayer} />
                    </li>
                  </div>
                );
              })}
            </ul>
          </div>

          <Menu />
        </main>
      )}
    </>
  );
};
export default withKeycloak(Dashboard);
