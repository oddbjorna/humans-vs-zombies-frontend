import withKeycloak from "../../hoc/withKeycloak";
import KeycloakService from "../../services/KeycloakService";
import styles from "./Profile.module.css";
import StarIcon from "@mui/icons-material/Star";
import StarBorderIcon from "@mui/icons-material/StarBorder";
import Menu from "../Navbar/Menu";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { killSetAction } from "../../store/actions/killActions";

/**
 * Profile Component which displays:
 * Player Rank
 * Faction
 * Amount of Humans Killed
 */
const Profile = () => {
  //Profile Variables
  const username = KeycloakService.getUsername();
  const user = JSON.parse(localStorage.getItem("user"));
  const [kills, setKills] = useState(0);
  const [isLoading, setIsLoading] = useState(true);
  const { kill } = useSelector((state) => state.killReducer);

  const dispatch = useDispatch();

  /**
   * Function that checks a users rank
   * pushes their rank value to an array
   * returns the array
   */
  const renderStars = () => {
    const arr = [];
    for (let i = 0; i < user.rank; i++) {
      arr.push(
        <li className={styles.star}>
          <StarIcon />
        </li>
      );
    }
    return arr;
  };

  /**
   * pushes the non-active stars to an array
   */
  const renderNonActiveStars = () => {
    const arr = [];
    for (let i = 0; i < 5 - user.rank; i++) {
      arr.push(
        <li className={styles.star}>
          <StarBorderIcon />
        </li>
      );
    }
    return arr;
  };

  //Dispatches the killSetAction from redux on render
  useEffect(() => {
    dispatch(killSetAction());
    setIsLoading(false);
  }, []);

  //checks how many kills a player has and sets the KillState to the length of the array.
  useEffect(() => {
    if (user) {
      if (!isLoading) {
        const playerKills = kill.filter((k) => k.killerId === user.playerId);
        setKills(playerKills.length);
      }
    }
  }, [kill]);

  return (
    <main className={styles.container}>
      {!user ? (
        <h1 className={styles.error}>
          Please Join a game to see your profile!
        </h1>
      ) : (
        <>
          <h1 className={styles.faction}>
            You are a {user.human ? "human" : "zombie"}
          </h1>
          <div className={styles.logo}>
            <img
              className={styles.image}
              src={`/ranks/${user.rank}@256px.png`}
              alt="rank"
            />
          </div>
          <h1 className={styles.username}>Username: {username}</h1>
          <div>
            <h2 className={styles.rank}>
              Rank:{" "}
              {user.zombie ? (
                <>
                  {user.rank === 1
                    ? "Recruit"
                    : user.rank === 2
                    ? "Soldier"
                    : user.rank === 3
                    ? "Captain"
                    : user.rank === 4
                    ? "Admiral"
                    : "General"}
                </>
              ) : (
                "human"
              )}
            </h2>
            <div>
              <ul className={styles.stars}>
                {renderStars().map((item) => item)}
                {renderNonActiveStars().map((item) => item)}
              </ul>
              <h3>
                {user.zombie
                  ? `You have bitten ${kills} ${
                      kills === 1 ? "human" : "humans"
                    }`
                  : ""}
              </h3>
            </div>
          </div>
          <Menu />
        </>
      )}
    </main>
  );
};
export default withKeycloak(Profile);
