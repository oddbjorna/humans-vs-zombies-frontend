import { Redirect } from "react-router-dom";
import KeycloakService from "../../services/KeycloakService";
import styles from "./Login.module.css";
import ArrowForwardIosRoundedIcon from "@mui/icons-material/ArrowForwardIosRounded";
/**
 * Login Component that is displayed when a user is logging in.
 */
const Login = () => {
  //Checks if the user is allready logged in, if true, the user is redirected to the dashboard
  if (KeycloakService.isLoggedIn()) {
    return <Redirect to="/dashboard" />;
  }

  //Onclick function that runs when the user clicks the login button
  const handleLoginClick = () => {
    KeycloakService.doLogin();
  };
  return (
    <main className={styles.mainContainer}>
      <div className={styles.headerContainer}>
        <h1>HUMANS VS ZOMBIES</h1>
        <p>Welcome Player!</p>
      </div>
      <div className={styles.buttonContainer}>
        <button onClick={handleLoginClick}>
          <ArrowForwardIosRoundedIcon />
        </button>
      </div>
    </main>
  );
};
export default Login;
