import { useState, useEffect } from "react";
import {
  MapContainer,
  TileLayer,
  Marker,
  Popup,
  useMapEvents,
  useMap,
  Pane,
  Rectangle
} from "react-leaflet";
import L from "leaflet";
import { useSelector, useDispatch } from "react-redux";
import { squadSetAction } from "../../store/actions/squadActions";
import styles from "./Map.module.css";
import APIService from "../../services/APIService";
import KeycloakService from "../../services/KeycloakService";
import Loader from "../Loader/Loader";
import { gameSetAction } from "../../store/actions/gameActions";
import { killSetAction } from "../../store/actions/killActions";
import { positionSetAction } from "../../store/actions/positionActions";
import moment from "moment";

/**
 * Map Component that is displayed in the Game Component
 */
const Map = (props) => {
  //Map Variables
  const [isLoading, setIsLoading] = useState(true);
  const user = JSON.parse(localStorage.getItem("user"));
  const { game } = useSelector((state) => state.gameReducer);
  const { squadCheckins } = useSelector((state) => state.squadReducer);
  const { kill } = useSelector((state) => state.killReducer);
  const { mission } = useSelector((state) => state.missionReducer);
  const { nwlat, nwlng, selat, selng } = useSelector(
    (state) => state.positionReducer
  );
  const [currentGame, setCurrentGame] = useState({
    nwLat: 0,
    nwLng: 0,
    seLat: 0,
    seLng: 0
  });
  const dispatch = useDispatch();

  const [selectedPosition, setSelectedPosition] = useState([0, 0]);

  const role = KeycloakService.hasRole(["admin"]);

  //Variable that contains the coordinate values that defines the play area.
  const bounds = [
    [currentGame.nwLat, currentGame.nwLng],
    [currentGame.seLat, currentGame.seLng]
  ];

  //Variable that contains the Green marker used for setting selected posioin
  const greenIcon = new L.Icon({
    iconUrl:
      "https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png",
    shadowUrl:
      "https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png",
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  //Variable that contains the Death Icon which is displayed on the map where a player is killed
  const deathIcon = new L.Icon({
    iconUrl: "/death.svg",
    shadowUrl:
      "https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png",
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  //Variable that contains the mission marker which is displayed on locations where there is a mission
  const missionIcon = new L.Icon({
    iconUrl: "/mission.svg",
    shadowUrl:
      "https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png",
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  //onClick function that sets the nwlat and nwlng coordinates
  const handleNWClick = () => {
    dispatch(
      positionSetAction({
        nwlat: selectedPosition[0],
        nwlng: selectedPosition[1],
        selat: selat,
        selng: selng
      })
    );
  };

  /**
   * onClick function that runs when a player tries to post a squad-checkin
   * runs the postSquadCheckin method from the APIService
   * pings the server
   * dispatches the squadSetAction from redux
   */
  const handleSquadCheckinClick = async () => {
    await APIService.postSquadCheckin(props.checkin);
    await props.connection.send("SendCheckin", { marker: "message" });
    dispatch(squadSetAction());
  };

  //MapEvents for Kills and Squad Check-ins
  const Markers = () => {
    useMapEvents({
      click(e) {
        setSelectedPosition([e.latlng.lat, e.latlng.lng]);
        if (!role) {
          props.handleCheckin({
            latitude: e.latlng.lat,
            longitude: e.latlng.lng
          });
          props.handleKill({
            latitude: e.latlng.lat,
            longitude: e.latlng.lng
          });
        }
      }
    });

    return selectedPosition ? (
      <Marker
        key={selectedPosition[0]}
        position={selectedPosition}
        icon={greenIcon}
        eventHandlers={{
          click: !role ? handleSquadCheckinClick : () => {}
        }}
      >
        <Popup>
          <p>{`LAT: ${selectedPosition[0]}`}</p>
          <p>{`LNG: ${selectedPosition[1]}`}</p>
          {role && (
            <div className={styles.posBtns}>
              <button className={styles.posBtn} onClick={handleNWClick}>
                Set Mission Location
              </button>
            </div>
          )}
        </Popup>
      </Marker>
    ) : null;
  };

  const ChangeView = () => {
    const map = useMap();
    map.setView(selectedPosition);
    return null;
  };

  /**
   * Uses geolocation to find your position and places a green marker on your postion
   * The device needs to have built in GPS to work.
   */
  useEffect(() => {
    navigator.geolocation.getCurrentPosition((position) => {
      const { latitude, longitude } = position.coords;
      setSelectedPosition([latitude, longitude]);
    });
    dispatch(gameSetAction());
    dispatch(killSetAction());

    setIsLoading(false);
  }, []);

  /**
   * Sets the CurrentGame value based on the users gameID
   * Sets the SelectedPostion values based on the current games coordinates
   */
  useEffect(() => {
    if (!isLoading) {
      setCurrentGame(game.filter((g) => g.id === user.gameId)[0]);
      setSelectedPosition([
        (currentGame.nwLat + currentGame.seLat) / 2,
        (currentGame.nwLng + currentGame.seLng) / 2
      ]);
    }
  }, [game]);

  return (
    <>
      {isLoading ? (
        <Loader height="70vh" width="100%" />
      ) : (
        <div className={styles.container}>
          <MapContainer
            className={styles.Map}
            center={selectedPosition}
            zoom={16}
            style={{ height: props.height, width: props.width }}
          >
            <Markers />
            {user.squadId &&
              squadCheckins.map((checkin) => {
                return (
                  checkin.squadId === user.squadId && (
                    <Marker
                      key={checkin.id}
                      position={[checkin.latitude, checkin.longitude]}
                    >
                      <Popup>
                        <h1>{`${checkin.username}`}</h1>
                        <h3> {moment(checkin.startTime).format("HH:mm")}</h3>
                      </Popup>
                    </Marker>
                  )
                );
              })}
            {kill.map((k) => {
              return (
                k.gameId === user.gameId && (
                  <Marker
                    key={k.id}
                    icon={deathIcon}
                    position={[k.latitude, k.longitude]}
                  >
                    <Popup>
                      <h1>{`${k.victimName}`}</h1>
                      <h3> {moment(k.timeOfDeath).format("HH:mm")}</h3>
                    </Popup>
                  </Marker>
                )
              );
            })}
            {mission.map((m) => {
              return (
                m.gameId === user.gameId && (
                  <Marker
                    key={m.name}
                    icon={missionIcon}
                    position={[m.latitude, m.longitude]}
                  >
                    <Popup>
                      <h1>{m.name}</h1>
                      <p>{m.description}</p>
                    </Popup>
                  </Marker>
                )
              );
            })}
            <ChangeView />
            <TileLayer
              attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <Pane style={{ zIndex: 499 }}>
              <Rectangle bounds={bounds} pathOptions={{ color: "green" }} />
            </Pane>
          </MapContainer>
        </div>
      )}
    </>
  );
};

export default Map;
