import { useState, useEffect } from "react";
import {
  MapContainer,
  TileLayer,
  Marker,
  Popup,
  useMapEvents,
  useMap,
  Pane,
  Rectangle,
} from "react-leaflet";
import L from "leaflet";
import { useSelector, useDispatch } from "react-redux";
import { squadSetAction } from "../../store/actions/squadActions";
import styles from "./DashboardMap.module.css";
import APIService from "../../services/APIService";
import KeycloakService from "../../services/KeycloakService";
import { gameSetAction } from "../../store/actions/gameActions";
import { killSetAction } from "../../store/actions/killActions";
import { useHistory, useLocation } from "react-router-dom";
import Loader from "../Loader/Loader";

/**
 * Dashboard Map Component
 * Simplified version of the map that is displayed in Dashboard GameCard
 * Shows the PlayArea of the map and killmarkers
 */
const DashboardMap = (props) => {
  //DashBoardMap Variables
  const [selectedPosition, setSelectedPosition] = useState([
    60.389811572433636, 5.332089309192921,
  ]);
  const [isLoading, setIsLoading] = useState(true);
  const user = JSON.parse(localStorage.getItem("user"));
  const { game } = useSelector((state) => state.gameReducer);
  const { squadMembers } = useSelector((state) => state.squadReducer);
  const { squadCheckins } = useSelector((state) => state.squadReducer);
  const { kill } = useSelector((state) => state.killReducer);
  const location = useLocation();
  const dispatch = useDispatch();

  const [currentGame, setCurrentGame] = useState({
    nwLat: 0,
    nwLng: 0,
    seLat: 0,
    seLng: 0,
  });

  //Variable that contains the coordinate values that defines the play area.
  const bounds = [
    [currentGame.nwLat, currentGame.nwLng],
    [currentGame.seLat, currentGame.seLng],
  ];

  //Variable that contains the Marker Icon for deceased players
  const deathIcon = new L.Icon({
    iconUrl: "/death.svg",
    shadowUrl:
      "https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png",
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41],
  });

  const ChangeView = () => {
    const map = useMap();
    map.setView(selectedPosition);
    return null;
  };

  /**
   * Uses geolocation to find your position and places a green marker on your postion
   * The device needs to have built in GPS to work.
   */
  useEffect(() => {
    navigator.geolocation.getCurrentPosition((position) => {
      const { latitude, longitude } = position.coords;
      setSelectedPosition([latitude, longitude]);
    });
    dispatch(gameSetAction());

    setIsLoading(false);
  }, []);

  //Dispatches several setActions
  useEffect(() => {
    setCurrentGame(props.game);
    dispatch(killSetAction());
    setSelectedPosition([
      (currentGame.nwLat + currentGame.seLat) / 2,
      (currentGame.nwLng + currentGame.seLng) / 2,
    ]);
  }, [game]);

  return (
    <>
      {isLoading ? (
        <Loader height="70vh" width="100%" />
      ) : (
        <div className={styles.disableMap}>
          <MapContainer
            className={styles.Map}
            center={selectedPosition}
            zoom={16}
            zoomControl={false}
          >
            {kill.map((k) => {
              return (
                k.gameId === props.game.id && (
                  <Marker
                    key={k.id}
                    icon={deathIcon}
                    position={[k.latitude, k.longitude]}
                  >
                    <Popup>
                      <h1>{`${k.victimName}`}</h1>
                    </Popup>
                  </Marker>
                )
              );
            })}
            <ChangeView />
            <TileLayer
              attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <Pane style={{ zIndex: 499 }}>
              <Rectangle bounds={bounds} pathOptions={{ color: "green" }} />
            </Pane>
          </MapContainer>
        </div>
      )}
    </>
  );
};

export default DashboardMap;
