import { applyMiddleware } from "redux";
import { gameMiddleware } from "./gameMiddleware";
import { userMiddleware } from "./userMiddleware";
import { chatMiddleware } from "./chatMiddleware";
import { killMiddleware } from "./killMiddleware";
import { playerMiddleware } from "./playerMiddleware";
import { sessionMiddleware } from "./sessionMiddleware";
import { squadMiddleware } from "./squadMiddleware";
import { missionMiddleware } from "./missionMiddleware";
import { connectionMiddleware } from "./connectionMiddleware";
import { positionMiddleware } from "./positionMiddleware";
export default applyMiddleware(
  gameMiddleware,
  userMiddleware,
  chatMiddleware,
  killMiddleware,
  playerMiddleware,
  sessionMiddleware,
  squadMiddleware,
  missionMiddleware,
  connectionMiddleware,
  positionMiddleware
);
