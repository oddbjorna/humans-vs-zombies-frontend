import APIService from "../../services/APIService";
import {
  ACTION_USER_SET,
  userErrorAction,
  userSuccessAction,
} from "../actions/userActions";

export const userMiddleware =
  ({ dispatch }) =>
  (next) =>
  async (action) => {
    next(action);

    if (action.type === ACTION_USER_SET) {
      try {
        const users = await APIService.getUsers();

        dispatch(userSuccessAction(users));
      } catch {
        dispatch(userErrorAction(action.payload));
      }
    }
  };
