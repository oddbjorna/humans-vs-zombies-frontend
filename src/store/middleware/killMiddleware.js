import APIService from "../../services/APIService";
import {
  ACTION_KILL_SET,
  killErrorAction,
  killSuccessAction,
} from "../actions/killActions";

export const killMiddleware =
  ({ dispatch }) =>
  (next) =>
  async (action) => {
    next(action);
    if (action.type === ACTION_KILL_SET) {
      try {
        const kills = await APIService.getKills();

        dispatch(killSuccessAction({ kills }));
      } catch {
        dispatch(killErrorAction(action.payload));
      }
    }
  };
