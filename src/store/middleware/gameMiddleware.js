import APIService from "../../services/APIService";
import {
  ACTION_GAME_SET,
  gameErrorAction,
  gameSuccessAction,
} from "../actions/gameActions";

export const gameMiddleware =
  ({ dispatch }) =>
  (next) =>
  async (action) => {
    next(action);

    if (action.type === ACTION_GAME_SET) {
      try {
        const games = await APIService.getGames();

        dispatch(gameSuccessAction(games));
      } catch (e) {
        dispatch(gameErrorAction(action.payload));
      }
    }
  };
