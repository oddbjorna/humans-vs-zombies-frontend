import { HubConnectionBuilder } from "@microsoft/signalr";
import {
  ACTION_CONNECTION_SET,
  connectionErrorAction,
  connectionSuccessAction
} from "../actions/connectionActions";

const API = "https://localhost:44385";
// const API = "https://humans-vs-zombies-api.azurewebsites.net";

export const connectionMiddleware =
  ({ dispatch }) =>
  (next) =>
  async (action) => {
    next(action);

    if (action.type === ACTION_CONNECTION_SET) {
      try {
        const newConnection = new HubConnectionBuilder()
          .withUrl(`${API}/hubs/game`)
          .withAutomaticReconnect()
          .build();

        dispatch(connectionSuccessAction(newConnection));
      } catch (e) {
        dispatch(connectionErrorAction(action.payload));
      }
    }
  };
