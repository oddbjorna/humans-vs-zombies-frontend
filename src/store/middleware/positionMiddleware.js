import APIService from "../../services/APIService";
import {
  ACTION_POSITION_SET,
  positionErrorAction,
  positionSuccessAction,
} from "../actions/positionActions";

export const positionMiddleware =
  ({ dispatch }) =>
  (next) =>
  async (action) => {
    next(action);

    if (action.type === ACTION_POSITION_SET) {
      try {
        dispatch(positionSuccessAction(action.payload));
      } catch {
        dispatch(positionErrorAction(action.payload));
      }
    }
  };
