import APIService from "../../services/APIService";

import {
    ACTION_MISSION_SET,
    missionErrorAction,
    missionSuccessAction,
    
} from "../actions/missionActions";

export const missionMiddleware = 
    ({dispatch}) =>
    (next) =>
    async (action) => {
        next(action);

        if(action.type === ACTION_MISSION_SET) {
            try {
                const missions = await APIService.getMissions();
                dispatch(missionSuccessAction(missions));
            } catch (e) {
                dispatch(missionErrorAction(action.payload));
                
            }
        }
    };