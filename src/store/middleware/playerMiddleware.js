import APIService from "../../services/APIService";
import {
  ACTION_PLAYER_SET,
  playerErrorAction,
  playerSuccessAction,
} from "../actions/playerActions";

export const playerMiddleware =
  ({ dispatch }) =>
  (next) =>
  async (action) => {
    next(action);

    if (action.type === ACTION_PLAYER_SET) {
      try {
        const players = await APIService.getPlayers();

        dispatch(playerSuccessAction(players));
      } catch {
        dispatch(playerErrorAction(action.payload));
      }
    }
  };
