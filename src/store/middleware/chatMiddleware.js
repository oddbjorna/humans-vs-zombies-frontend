import APIService from "../../services/APIService";
import {
  ACTION_CHAT_SET,
  chatErrorAction,
  chatSuccessAction,
} from "../actions/chatActions";

export const chatMiddleware =
  ({ dispatch }) =>
  (next) =>
  async (action) => {
    next(action);

    if (action.type === ACTION_CHAT_SET) {
      try {
        const user = JSON.parse(localStorage.getItem("user"));

        const globalChat = await APIService.getGlobalChat(user.gameId);

        let squadChat;
        if (user.squadId) {
          squadChat = await APIService.getSquadChat(user.squadId);
        } else {
          squadChat = [];
        }

        const factionChat = await APIService.getFactionChat(user.gameId);

        dispatch(
          chatSuccessAction({
            ...action.payload,
            globalChat: globalChat,
            squadChat: squadChat,
            factionChat: factionChat,
          })
        );
      } catch (e) {
        dispatch(chatErrorAction(action.payload));
      }
    }
  };
