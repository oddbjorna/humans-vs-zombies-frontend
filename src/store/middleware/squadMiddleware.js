import APIService from "../../services/APIService";
import {
  ACTION_SQUAD_SET,
  squadErrorAction,
  squadSuccessAction,
} from "../actions/squadActions";

export const squadMiddleware =
  ({ dispatch }) =>
  (next) =>
  async (action) => {
    next(action);

    if (action.type === ACTION_SQUAD_SET) {
      try {
        const squads = await APIService.getSquads();
        const squadCheckins = await APIService.getSquadCheckins();
        const squadMembers = await APIService.getSquadMembers();

        dispatch(
          squadSuccessAction({
            squads,
            squadCheckins,
            squadMembers,
          })
        );
      } catch {
        dispatch(squadErrorAction(action.payload));
      }
    }
  };
