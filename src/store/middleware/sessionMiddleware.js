import APIService from "../../services/APIService";
import KeycloakService from "../../services/KeycloakService";
import {
  ACTION_SESSION_SET,
  sessionErrorAction,
  sessionSuccessAction,
  ACTION_SESSION_UPDATE,
  ACTION_SESSION_SET_SQUAD,
  ACTION_SESSION_SET_KILL,
  ACTION_SESSION_CLEAR,
  ACTION_SESSION_UPDATE_RANK,
} from "../actions/sessionActions";

export const sessionMiddleware =
  ({ dispatch }) =>
  (next) =>
  async (action) => {
    next(action);

    if (action.type === ACTION_SESSION_SET) {
      try {
        const users = await APIService.getUsers();
        const user = users.filter(
          (u) => u.username === KeycloakService.getUsername()
        )[0];

        const players = await APIService.getPlayers();
        const player = players.filter((p) => p.userId === user.id)[0];

        const games = await APIService.getGames();
        const game = games.filter((g) => g.id === player.gameId)[0];

        const sessionUser = {
          userId: user.id,
          username: KeycloakService.getUsername(),
          playerId: player.id,
          gameId: player.gameId,
          gameName: game.name,
          human: player.isHuman,
          zombie: !player.isHuman,
          rank: player.rank,
        };

        localStorage.setItem("user", JSON.stringify(sessionUser));

        dispatch(sessionSuccessAction(sessionUser));
      } catch (error) {
        dispatch(sessionErrorAction(action.payload));
      }
    }

    if (action.type === ACTION_SESSION_UPDATE) {
      try {
        const user = JSON.parse(localStorage.getItem("user"));
        await APIService.deleteSquadMember(user.playerId);
        delete user.squadMemberId;
        delete user.squadId;
        localStorage.setItem("user", JSON.stringify(user));

        dispatch(sessionSuccessAction(user));
      } catch {
        dispatch(sessionErrorAction(action.payload));
      }
    }
    if (action.type === ACTION_SESSION_SET_SQUAD) {
      try {
        const user = JSON.parse(localStorage.getItem("user"));
        if (!user.squadId) {
          const response = await APIService.postSquadMember(action.payload.id);
          localStorage.setItem(
            "user",
            JSON.stringify({
              ...user,
              squadId: action.payload.id,
              squadMemberId: response.id,
            })
          );
        }
        const sessionUser = JSON.parse(localStorage.getItem("user"));
        dispatch(sessionSuccessAction(sessionUser));
      } catch {
        dispatch(sessionErrorAction(action.payload));
      }
    }
    // hjelp???
    if (action.type === ACTION_SESSION_SET_KILL) {
      try {
        const user = JSON.parse(localStorage.getItem("user"));
        if (user.isHuman) {
          const response = await APIService.postKill(action.payload.id);
          localStorage.setItem(
            "user",
            JSON.stringify({
              ...user,
              killId: action.payload.id,
              playerId: response.id,
              isHuman: false,
            })
          );
          const sessionKill = JSON.parse(localStorage.getItem("user"));
          dispatch(sessionSuccessAction(sessionKill));
        }
      } catch {
        dispatch(sessionErrorAction(action.payload));
      }
    }
    if (action.type === ACTION_SESSION_UPDATE_RANK) {
      try {
        const user = JSON.parse(localStorage.getItem("user"));
        const players = await APIService.getPlayers();
        const player = players.filter((p) => p.id === user.playerId)[0];

        localStorage.setItem(
          "user",
          JSON.stringify({
            ...user,
            rank: player.rank,
          })
        );
        const sessionUser = JSON.parse(localStorage.getItem("user"));
        dispatch(sessionSuccessAction(sessionUser));
      } catch {
        dispatch(sessionErrorAction(action.payload));
      }
    }
    if (action.type === ACTION_SESSION_CLEAR) {
      try {
        dispatch(sessionSuccessAction(""));
      } catch {
        dispatch(sessionErrorAction(action.payload));
      }
    }
  };
