export const ACTION_MISSION_SET = '[mission] SET';
export const ACTION_MISSION_DELETE = '[mission] DELETE';
export const ACTION_MISSION_ERROR = '[mission] ERROR';
export const ACTION_MISSION_SUCCESS = '[mission] SUCCESS';


export const missionSetAction = (mission) => ({
    type: ACTION_MISSION_SET,
    payload: mission
});

export const missionErrorAction = (error) => ({
    type: ACTION_MISSION_ERROR,
    payload: error
});

export const missionSuccessAction = (success) => ({
    type: ACTION_MISSION_SUCCESS,
    payload: success
});

export const missionDeleteAction = (mission) => ({
    type: ACTION_MISSION_DELETE,
    payload: mission
});
