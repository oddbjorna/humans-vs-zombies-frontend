export const ACTION_CONNECTION_SET = "[connection] SET";
export const ACTION_CONNECTION_ERROR = "[connection] ERROR";
export const ACTION_CONNECTION_SUCCESS = "[connection] SUCCESS";

export const connectionSetAction = (connection) => ({
  type: ACTION_CONNECTION_SET,
  payload: connection,
});

export const connectionErrorAction = (error) => ({
  type: ACTION_CONNECTION_ERROR,
  payload: error,
});

export const connectionSuccessAction = (success) => ({
  type: ACTION_CONNECTION_SUCCESS,
  payload: success,
});
