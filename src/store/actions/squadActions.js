export const ACTION_SQUAD_SET = "[squad] SET";
export const ACTION_SQUAD_ERROR = "[squad] ERROR";
export const ACTION_SQUAD_SUCCESS = "[squad] SUCCESS";

export const squadSetAction = (id) => ({
  type: ACTION_SQUAD_SET,
  payload: id,
});

export const squadSuccessAction = (success) => ({
  type: ACTION_SQUAD_SUCCESS,
  payload: success,
});

export const squadErrorAction = (error) => ({
  type: ACTION_SQUAD_ERROR,
  payload: error,
});
