export const ACTION_PLAYER_SET = "[player] SET";
export const ACTION_PLAYER_ERROR = "[player] ERROR";
export const ACTION_PLAYER_DELETE = "[player] DELETE";
export const ACTION_PLAYER_SUCCESS = "[player] SUCCESS";

export const playerSetAction = (id) => ({
  type: ACTION_PLAYER_SET,
  payload: id,
});

export const playerSuccessAction = (success) => ({
  type: ACTION_PLAYER_SUCCESS,
  payload: success,
});

export const playerErrorAction = (error) => ({
  type: ACTION_PLAYER_ERROR,
  payload: error,
});

export const playerDeleteAction = (player) => ({
  type: ACTION_PLAYER_DELETE,
  payload: player,
});
