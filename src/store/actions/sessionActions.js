export const ACTION_SESSION_SET = "[session] SET";
export const ACTION_SESSION_ERROR = "[session] ERROR";
export const ACTION_SESSION_SUCCESS = "[session] SUCCESS";
export const ACTION_SESSION_UPDATE = "[session] UPDATE";
export const ACTION_SESSION_SET_SQUAD = "[session] SET_SQUAD";
export const ACTION_SESSION_SET_KILL = "[session] SET_KILL";
export const ACTION_SESSION_CLEAR = "[session] CLEAR";
export const ACTION_SESSION_UPDATE_RANK = "[session] UPDATE RANK";

export const sessionSetAction = (id) => ({
  type: ACTION_SESSION_SET,
  payload: id,
});

export const sessionSuccessAction = (success) => ({
  type: ACTION_SESSION_SUCCESS,
  payload: success,
});

export const sessionErrorAction = (error) => ({
  type: ACTION_SESSION_ERROR,
  payload: error,
});

export const sessionUpdateAction = (update) => ({
  type: ACTION_SESSION_UPDATE,
  payload: update,
});

export const sessionSetSquadAction = (squad) => ({
  type: ACTION_SESSION_SET_SQUAD,
  payload: squad,
});

export const sessionSetKillAction = (kill) => ({
  type: ACTION_SESSION_SET_KILL,
  payload: kill,
});

export const sessionUpdateRankAction = (update) => ({
  type: ACTION_SESSION_UPDATE_RANK,
  payload: update,
});

export const sessionClearAction = (session) => ({
  type: ACTION_SESSION_CLEAR,
  payload: session,
});
