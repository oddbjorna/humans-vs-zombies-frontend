export const ACTION_GAME_SET = '[game] SET';
export const ACTION_GAME_ERROR = '[game] ERROR';
export const ACTION_GAME_SUCCESS = '[game] SUCCESS';
export const ACTION_GAME_DELETE = '[game] DELETE';

export const gameSetAction = (game) => ({
  type: ACTION_GAME_SET,
  payload: game
});

export const gameErrorAction = (error) => ({
  type: ACTION_GAME_ERROR,
  payload: error
});

export const gameSuccessAction = (sucess) => ({
  type: ACTION_GAME_SUCCESS,
  payload: sucess
});

export const gameDeleteAction = (game) => ({
  type: ACTION_GAME_DELETE,
  payload: game
});
