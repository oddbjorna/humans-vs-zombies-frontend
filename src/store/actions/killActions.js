export const ACTION_KILL_SET = '[kill] SET';
export const ACTION_KILL_ERROR = '[kill] ERROR';
export const ACTION_KILL_SUCCESS = '[kill] SUCCESS';
export const ACTION_KILL_DELETE = '[kill] DELETE';

export const killSetAction = (kill) => ({
  type: ACTION_KILL_SET,
  payload: kill
});

export const killErrorAction = (error) => ({
  type: ACTION_KILL_ERROR,
  payload: error
});

export const killSuccessAction = (success) => ({
  type: ACTION_KILL_SUCCESS,
  payload: success
});

export const killDeleteAction = (kill) => ({
  type: ACTION_KILL_DELETE,
  payload: kill
});
