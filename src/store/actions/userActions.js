export const ACTION_USER_SET = "[user] SET";
export const ACTION_USER_ERROR = "[user] ERROR";
export const ACTION_USER_SUCCESS = "[user] SUCCESS";

export const userSetAction = (id) => ({
  type: ACTION_USER_SET,
  payload: id,
});

export const userSuccessAction = (success) => ({
  type: ACTION_USER_SUCCESS,
  payload: success,
});

export const userErrorAction = (error) => ({
  type: ACTION_USER_ERROR,
  payload: error,
});
