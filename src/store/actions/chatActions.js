export const ACTION_CHAT_SET = '[chat] SET';
export const ACTION_CHAT_ERROR = '[chat] ERROR';
export const ACTION_CHAT_SUCCESS = '[chat] SUCCESS';

export const chatSetAction = (chat) => ({
  type: ACTION_CHAT_SET,
  payload: chat
});

export const chatErrorAction = (error) => ({
  type: ACTION_CHAT_ERROR,
  payload: error
});

export const chatSuccessAction = (success) => ({
  type: ACTION_CHAT_SUCCESS,
  payload: success
});
