export const ACTION_POSITION_SET = "[position] SET";
export const ACTION_POSITION_ERROR = "[position] ERROR";
export const ACTION_POSITION_SUCCESS = "[position] SUCCESS";

export const positionSetAction = (id) => ({
  type: ACTION_POSITION_SET,
  payload: id,
});

export const positionSuccessAction = (success) => ({
  type: ACTION_POSITION_SUCCESS,
  payload: success,
});

export const positionErrorAction = (error) => ({
  type: ACTION_POSITION_ERROR,
  payload: error,
});
