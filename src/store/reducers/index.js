import { combineReducers } from "redux";
import { gameReducer } from "./gameReducer";
import { userReducer } from "./userReducer";
import { chatReducer } from "./chatReducer";
import { killReducer } from "./killReducer";
import { playerReducer } from "./playerReducer";
import { sessionReducer } from "./sessionReducer";
import { squadReducer } from "./squadReducer";
import { missionReducer } from "./missionReducer";
import { connectionReducer } from "./connectionReducer";
import { positionReducer } from "./positionReducer";

const appReducer = combineReducers({
  gameReducer,
  userReducer,
  chatReducer,
  killReducer,
  playerReducer,
  sessionReducer,
  squadReducer,
  missionReducer,
  connectionReducer,
  positionReducer,
});

export default appReducer;
