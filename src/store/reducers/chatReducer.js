import {
  ACTION_CHAT_ERROR,
  ACTION_CHAT_SET,
  ACTION_CHAT_SUCCESS,
} from "../actions/chatActions";

const initialState = {
  globalChat: [],
  squadChat: [],
  factionChat: [],
  error: "",
};

export const chatReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_CHAT_SET:
      return {
        ...state,
      };
    case ACTION_CHAT_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    case ACTION_CHAT_SUCCESS:
      return {
        ...state,
        globalChat: action.payload.globalChat,
        squadChat: action.payload.squadChat,
        factionChat: action.payload.factionChat,
      };
    default:
      return state;
  }
};
