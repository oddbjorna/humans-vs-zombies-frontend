import {
  ACTION_CONNECTION_ERROR,
  ACTION_CONNECTION_SET,
  ACTION_CONNECTION_SUCCESS,
} from "../actions/connectionActions";

const initialState = {
  connection: null,
  error: "",
};

export const connectionReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_CONNECTION_SET:
      return {
        ...state,
      };
    case ACTION_CONNECTION_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    case ACTION_CONNECTION_SUCCESS:
      return {
        ...state,
        connection: action.payload,
      };
    default:
      return state;
  }
};
