import {
  ACTION_USER_ERROR,
  ACTION_USER_SET,
  ACTION_USER_SUCCESS,
} from "../actions/userActions";

const initialState = {
  user: [],
  error: "",
};

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_USER_SET:
      return {
        ...state,
      };
    case ACTION_USER_SUCCESS:
      return {
        ...state,
        user: action.payload,
      };
    case ACTION_USER_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
};
