import {
  ACTION_GAME_DELETE,
  ACTION_GAME_ERROR,
  ACTION_GAME_SET,
  ACTION_GAME_SUCCESS,
} from "../actions/gameActions";

const initialState = {
  game: [],
  error: "",
};

export const gameReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_GAME_SET:
      return {
        ...state,
      };
    case ACTION_GAME_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    case ACTION_GAME_SUCCESS:
      return {
        ...state,
        game: action.payload,
      };
    case ACTION_GAME_DELETE:
      return {
        ...state,
      };
    default:
      return state;
  }
};
