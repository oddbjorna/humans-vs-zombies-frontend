import {
  ACTION_POSITION_ERROR,
  ACTION_POSITION_SUCCESS,
} from "../actions/positionActions";

const initialState = {
  nwlat: 0,
  nwlng: 0,
  selat: 0,
  selng: 0,
  error: "",
};

export const positionReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_POSITION_SUCCESS:
      return {
        ...state,
        nwlat: action.payload.nwlat,
        nwlng: action.payload.nwlng,
        selat: action.payload.selat,
        selng: action.payload.selng,
      };
    case ACTION_POSITION_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
};
