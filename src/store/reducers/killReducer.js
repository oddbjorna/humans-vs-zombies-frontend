import {
  ACTION_KILL_DELETE,
  ACTION_KILL_ERROR,
  ACTION_KILL_SUCCESS,
} from "../actions/killActions";

const initialState = {
  kill: [],
  error: "",
};

export const killReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_KILL_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    case ACTION_KILL_SUCCESS:
      return {
        ...state,
        kill: action.payload.kills,
      };
    case ACTION_KILL_DELETE:
      return {
        ...state,
      };
    default:
      return state;
  }
};
