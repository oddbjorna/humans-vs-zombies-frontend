import {
  ACTION_SESSION_ERROR,
  ACTION_SESSION_SUCCESS,
} from "../actions/sessionActions";

const initialState = {
  session: "",
  error: "",
};

export const sessionReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_SESSION_SUCCESS:
      return {
        ...state,
        session: action.payload,
      };
    case ACTION_SESSION_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
};
