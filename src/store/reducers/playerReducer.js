import {
  ACTION_PLAYER_DELETE,
  ACTION_PLAYER_ERROR,
  ACTION_PLAYER_SET,
  ACTION_PLAYER_SUCCESS,
} from "../actions/playerActions";

const initialState = {
  player: [],
  error: "",
};

export const playerReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_PLAYER_SET:
      return {
        ...state,
      };
    case ACTION_PLAYER_SUCCESS:
      return {
        ...state,
        player: [...action.payload],
      };
    case ACTION_PLAYER_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    case ACTION_PLAYER_DELETE:
      return {
        ...state,
      };
    default:
      return state;
  }
};
