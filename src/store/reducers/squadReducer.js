import {
  ACTION_SQUAD_ERROR,
  ACTION_SQUAD_SET,
  ACTION_SQUAD_SUCCESS
} from '../actions/squadActions';

const initialState = {
  squad: [],
  squadCheckins: [],
  error: '',
  squadMembers: []
};

export const squadReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_SQUAD_SET:
      return {
        ...state
      };
    case ACTION_SQUAD_SUCCESS:
      return {
        ...state,
        squad: action.payload.squads,
        squadCheckins: action.payload.squadCheckins,
        squadMembers: action.payload.squadMembers
      };
    case ACTION_SQUAD_ERROR:
      return {
        ...state,
        error: action.payload
      };
    default:
      return state;
  }
};
