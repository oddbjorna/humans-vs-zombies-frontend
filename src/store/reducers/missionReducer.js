import {
    ACTION_MISSION_DELETE,
    ACTION_MISSION_SET,
    ACTION_MISSION_ERROR,
    ACTION_MISSION_SUCCESS,
} from "../actions/missionActions";

const initialState = {
    mission: [],
    error:"",
}

export const missionReducer = (state = initialState, action) => {
    switch (action.type){
        case ACTION_MISSION_SET:
            return{
                ...state,
            };
        case ACTION_MISSION_ERROR:
            return{
                ...state,
                error: action.payload,
            };
        case ACTION_MISSION_SUCCESS:
            return {
                ...state,
                mission: action.payload,
            };
        case ACTION_MISSION_DELETE:
            return{
                ...state,
            };
        default:
            return state;
    }
};