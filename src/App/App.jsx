import "./App.css";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import Dashboard from "../features/Dashboard/Dashboard";
import Login from "../features/Login/Login";
import Navbar from "../features/Navbar/Navbar";
import Profile from "../features/Profile/Profile";
import Game from "../features/Game/Game";
import AdminProfile from "../features/Admin Profile/AdminProfile";
import CreateAdminGame from "../features/Admin Profile/Game/CreateAdminGame";
import AdminMission, {
  CreateAdminMission,
} from "../features/Admin Profile/Mission/CreateAdminMission";
import AdminPlayer from "../features/Admin Profile/Player/AdminPlayer";
import EditAdminGame from "../features/Admin Profile/Game/EditAdminGame";
import EditAdminMission from "../features/Admin Profile/Mission/EditAdminMission";

const App = () => {
  return (
    <BrowserRouter>
      <Navbar />
      <Switch>
        <Route exact path="/">
          <Redirect to="/login" />
        </Route>
        <Route path="/login" component={Login} />
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/profile" component={Profile} />
        <Route path="/game" component={Game} />
        <Route exact path="/admin" component={AdminProfile} />
        <Route exact path="/creategame" component={CreateAdminGame} />
        <Route exact path="/editgame" component={EditAdminGame} />
        <Route exact path="/createmission" component={CreateAdminMission} />
        <Route exact path="/editmission" component={EditAdminMission} />
        <Route exact path="/adminplayer" component={AdminPlayer} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
