import KeycloakService from "./KeycloakService";
import moment from "moment";

const API = "https://localhost:44385/api";
// const API = "https://humans-vs-zombies-api.azurewebsites.net/api";

const APIService = {
  //API requests for Games

  /**
   * Gets all games from the database, returns Json objects with a list of games
   */
  getGames: async () => {
    const requestOptions = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      }
    };
    const apiGames = await fetch(`${API}/Games`, requestOptions);
    return apiGames.json();
  },

  /**
   * Posts a new game to the database.
   */
  postGame: async (Name, description, NwLat, NwLng, SeLat, SeLng) => {
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      },
      body: JSON.stringify({
        Name: Name,
        description: description,
        GameState: "Registration",
        NwLat: NwLat,
        NwLng: NwLng,
        SeLat: SeLat,
        SeLng: SeLng
      })
    };
    const response = await fetch(`${API}/Games`, requestOptions);
    return response.json();
  },

  /**
   * Edits a Game's gamestate, takes in gameId and wanted gamestate as parameters.
   */
  editGame: async (gameId, GameState) => {
    const preFetch = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      }
    };
    const getGame = await fetch(`${API}/Games/${gameId}`, preFetch);
    const game = await getGame.json();
    const requestOptions = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      },
      body: JSON.stringify({
        id: parseInt(gameId),
        name: game.name,
        description: game.description,
        gameState: GameState,
        nwLat: game.nwLat,
        nwLng: game.nwLng,
        seLat: game.seLat,
        seLng: game.seLng
      })
    };
    await fetch(`${API}/Games/${gameId}`, requestOptions);
  },

  deleteGame: async (id) => {
    const requestOptions = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      }
    };
    try {
      await fetch(`${API}/Games/${id}`, requestOptions);
    } catch (e) {
      console.log(e.message);
    }
  },

  // API requests for Missions

  /**
   * Gets all missions from the database
   * Returns a json object containing all the missions
   */
  getMissions: async () => {
    const requestOptions = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      }
    };
    const apiMissions = await fetch(`${API}/Missions`, requestOptions);
    return apiMissions.json();
  },

  /**
   * Posts a new mission to the database.
   * Takes the mission values as parameters.
   */
  postMission: async (
    missionname,
    description,
    latitude,
    longitude,
    humanvisible,
    zombievisible,
    gameId
  ) => {
    const time = moment().format();
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      },
      body: JSON.stringify({
        name: missionname,
        isHumanVisible: humanvisible,
        IsZombieVisible: zombievisible,
        description: description,
        latitude: latitude,
        longitude: longitude,
        startTime: time,
        endTime: time,
        gameId: gameId
      })
    };
    const response = await fetch(`${API}/Missions`, requestOptions);
    return response.json();
  },

  /**
   * Edits the mission description of a mission
   * Takes missionId and wanted description change as parameters
   */
  editMission: async (missionId, description) => {
    const preFetch = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      }
    };
    const getMission = await fetch(`${API}/Missions/${missionId}`, preFetch);
    const mission = await getMission.json();
    const requestOptions = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      },
      body: JSON.stringify({
        id: parseInt(missionId),
        name: mission.name,
        isHumanVisible: mission.isHumanVisible,
        IsZombieVisible: mission.IsZombieVisible,
        description: description,
        latitude: mission.latitude,
        longitude: mission.longitude,
        startTime: mission.startTime,
        endTime: mission.endTime,
        gameId: mission.gameId
      })
    };
    await fetch(`${API}/Missions/${missionId}`, requestOptions);
  },

  /**
   * Deletes a mission from the database
   * Takes the missionId as parameter
   */
  deleteMission: async (id) => {
    const requestOptions = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      }
    };
    try {
      await fetch(`${API}/Missions/${id}`, requestOptions);
    } catch (e) {
      console.log(e.message);
    }
  },

  // API Requests for User

  /**
   * Fetches all the users from the database
   * returns a Json object containing all the users.
   */
  getUsers: async () => {
    const requestOptions = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      }
    };
    const apiUsers = await fetch(`${API}/Users`, requestOptions);
    return apiUsers.json();
  },

  /**
   * Posts a new user to the database.
   * takes the username as parameter.
   */
  postUser: async (username) => {
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      },
      body: JSON.stringify({
        username: username
      })
    };
    const response = await fetch(`${API}/Users`, requestOptions);
    return response.json();
  },

  // API Requests for Players

  /**
   * Fetches all the Players from the database
   * returns a JSON object containg all the players
   */
  getPlayers: async () => {
    const requestOptions = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      }
    };
    const apiPlayers = await fetch(`${API}/Players`, requestOptions);
    return apiPlayers.json();
  },

  /**
   * Posts a new player to the database
   * takes userID and gameID of game you want to join as parameters.
   * bitecode: generates a random string that is later used for registering kills
   * random _boolean: variable used to decide if player is zombie or human
   */
  postPlayer: async (userId, gameId) => {
    const bitecode = (Math.random() + 1).toString(36).substring(7);
    const random_boolean = Math.random() < 0.7;
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      },
      body: JSON.stringify({
        isHuman: random_boolean,
        isPatientZero: !random_boolean,
        biteCode: bitecode,
        userId: userId,
        gameId: gameId,
        username: KeycloakService.getUsername(),
        rank: 1
      })
    };
    const response = await fetch(`${API}/Players`, requestOptions);
    return response.json();
  },

  /**
   * Deletes a player from the database
   * Takes players id as parameter
   */
  deletePlayer: async (id) => {
    const requestOptions = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      }
    };
    try {
      await fetch(`${API}/Players/${id}`, requestOptions);
    } catch (e) {
      console.log(e.message);
    }
  },

  /**
   * Edits a players state (Human/Zombie)
   */
  editPlayer: async (playerId, isHuman) => {
    const preFetch = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      }
    };
    const getPLayer = await fetch(`${API}/Players/${playerId}`, preFetch);
    const player = await getPLayer.json();
    console.log(player);
    const requestOptions = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      },
      body: JSON.stringify({
        id: parseInt(playerId),
        isHuman: isHuman,
        isPatientZero: player.isPatientZero,
        biteCode: player.biteCode,
        userId: player.userId,
        gameId: player.gameId,
        rank: player.rank,
        username: player.username
      })
    };
    await fetch(`${API}/Players/${playerId}`, requestOptions);
  },

  /**
   * Changes a humans state to zombie.
   * takes the players id as parameter
   */
  setZombie: async (playerId) => {
    const preFetch = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      }
    };
    const getPLayer = await fetch(`${API}/Players/${playerId}`, preFetch);
    const player = await getPLayer.json();
    const requestOptions = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      },
      body: JSON.stringify({
        id: parseInt(playerId),
        isHuman: false,
        isPatientZero: player.isPatientZero,
        biteCode: player.biteCode,
        userId: player.userId,
        rank: player.rank,
        gameId: player.gameId,
        username: player.username
      })
    };
    await fetch(`${API}/Players/${playerId}`, requestOptions);
  },

  /**
   * Increments a players rank by 1
   */
  increaseRank: async (playerId) => {
    const getPlayers = await APIService.getPlayers();
    const player = getPlayers.filter((p) => p.id === playerId)[0];
    const requestOptions = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      },
      body: JSON.stringify({
        id: playerId,
        isHuman: player.isHuman,
        isPatientZero: player.isPatientZero,
        biteCode: player.biteCode,
        userId: player.userId,
        rank: player.rank + 1,
        gameId: player.gameId,
        username: player.username
      })
    };
    await fetch(`${API}/Players/${playerId}`, requestOptions);
  },

  // API Requests for Chat

  /**
   * Fetches all messages from the Global Chat
   * Returns a JSON object containing all the messages
   */
  getGlobalChat: async (id) => {
    const requestOptions = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      }
    };
    const response = await fetch(`${API}/Games/${id}/chat`, requestOptions);
    return response.json();
  },

  /**
   * Fetches all messages from the Squad Chat
   * Returns a JSON object containing all the messages
   */
  getSquadChat: async (id) => {
    const requestOptions = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      }
    };
    const response = await fetch(`${API}/Squads/${id}/chat`, requestOptions);
    return response.json();
  },

  /**
   * Fetches all messages from the Faction Chat
   * Returns a JSON object containing all the messages
   */
  getFactionChat: async (id) => {
    const requestOptions = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      }
    };
    const user = JSON.parse(localStorage.getItem("user"));
    let response;
    if (user.human) {
      response = await fetch(`${API}/Games/${id}/chat/Human`, requestOptions);
    } else if (user.zombie) {
      response = await fetch(`${API}/Games/${id}/chat/Zombie`, requestOptions);
    }
    return response.json();
  },

  /**
   * Posts a message to Global Chat
   * takes the message as parameter
   */
  postGlobalChat: async (message) => {
    const time = moment().format();
    const user = JSON.parse(localStorage.getItem("user"));

    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      },
      body: JSON.stringify({
        username: KeycloakService.getUsername(),
        message: message,
        isHumanGlobal: false,
        isZombieGlobal: false,
        chatTime: time,
        gameId: user.gameId,
        userId: user.userId,
        squadMemberId: null,
        squadId: null
      })
    };
    const response = await fetch(
      `${API}/Games/${user.gameId}/chat`,
      requestOptions
    );
    return response.json();
  },

  /**
   * Posts a message to Squad Chat
   * takes the message as parameter
   */
  postSquadChat: async (message) => {
    const time = moment().format();
    const user = JSON.parse(localStorage.getItem("user"));
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      },
      body: JSON.stringify({
        username: KeycloakService.getUsername(),
        message: message,
        isHumanGlobal: false,
        isZombieGlobal: false,
        chatTime: time,
        gameId: user.gameId,
        userId: user.userId,
        squadMemberId: user.squadMemberId,
        squadId: user.squadId
      })
    };
    const response = await fetch(
      `${API}/Squads/${user.squadId}/chat`,
      requestOptions
    );
    return response.json();
  },

  /**
   * Posts a message to Faction Chat
   * takes the message as parameter
   */
  postFactionChat: async (message) => {
    const time = moment().format();
    const user = JSON.parse(localStorage.getItem("user"));

    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      },
      body: JSON.stringify({
        username: KeycloakService.getUsername(),
        message: message,
        isHumanGlobal: user.human,
        isZombieGlobal: user.zombie,
        chatTime: time,
        gameId: user.gameId,
        userId: user.userId,
        squadMemberId: null,
        squadId: null
      })
    };
    const response = await fetch(
      `${API}/Games/${user.gameId}/chat`,
      requestOptions
    );
    return response.json();
  },

  // API Requests for Squad

  /**
   * Fetches all squads from the database
   * returns a JSON object containing all the squads
   */
  getSquads: async () => {
    const requestOptions = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      }
    };
    const squads = await fetch(`${API}/Squads`, requestOptions);
    return squads.json();
  },

  /**
   * Fetches all squadmembers from the database
   * returns a JSON object containing all the squadmembers
   */
  getSquadMembers: async () => {
    const requestOptions = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      }
    };
    const squadMembers = await fetch(
      `${API}/Squads/squad_members`,
      requestOptions
    );
    return squadMembers.json();
  },

  /**
   * fetches all squad check-ins from the database
   * returns a JSON object containing all the squad check-ins
   */
  getSquadCheckins: async () => {
    const requestOptions = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      }
    };
    const checkins = await fetch(`${API}/Squads/check-in`, requestOptions);
    return checkins.json();
  },

  /**
   * Posts a squad check-in.
   * takes the checkin values as parameters
   */
  postSquadCheckin: async (checkin) => {
    const time = moment().format();
    const user = JSON.parse(localStorage.getItem("user"));
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      },
      body: JSON.stringify({
        username: KeycloakService.getUsername(),
        startTime: time,
        endTime: time,
        latitude: checkin.latitude,
        longitude: checkin.longitude,
        gameId: user.gameId,
        squadId: user.squadId,
        squadMemberId: user.squadMemberId
      })
    };
    const response = await fetch(
      `${API}/Squads/${user.squadId}/check-in`,
      requestOptions
    );
    return response.json();
  },

  /**
   * Posts a new squadmember.
   * takes the Id of the squad the player is joining in the parameters.
   */
  postSquadMember: async (squadId) => {
    const user = JSON.parse(localStorage.getItem("user"));
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      },
      body: JSON.stringify({
        username: KeycloakService.getUsername(),
        gameId: user.gameId,
        squadId: squadId,
        playerId: user.playerId,
        isHuman: user.human
      })
    };
    const response = await fetch(
      `${API}/Squads/${squadId}/join`,
      requestOptions
    );
    return response.json();
  },

  /**
   * Deletes a squadmember
   * takes the squadmembers id as parameter.
   */
  deleteSquadMember: async (squadMemberId) => {
    const requestOptions = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      }
    };
    await fetch(`${API}/Squads/SquadMember/${squadMemberId}`, requestOptions);
  },

  /**
   * Posts a new squad
   * takes the Squad Name as parameter
   */
  postSquad: async (squadName) => {
    const user = JSON.parse(localStorage.getItem("user"));
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      },
      body: JSON.stringify({
        name: squadName,
        ishuman: user.human,
        gameId: user.gameId,
        deceased: 0
      })
    };
    const response = await fetch(`${API}/Squads`, requestOptions);
    return response.json();
  },
  /**
   * Increases the number of deceased players in a squad.
   * increments the number of deceased by 1.
   */
  increaseDeceasedSquad: async (squadId) => {
    const preFetch = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      }
    };
    const getSquad = await fetch(`${API}/Squads/${squadId}`, preFetch);
    const squad = await getSquad.json();
    console.log(squad);
    const requestOptions = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      },
      body: JSON.stringify({
        id: parseInt(squadId),
        name: squad.name,
        isHuman: squad.isHuman,
        deceased: parseInt(squad.deceased) + 1,
        gameId: squad.gameId
      })
    };
    await fetch(`${API}/Squads/${squadId}`, requestOptions);
  },

  /**
   * Deletes a squad
   * Takes the squad Id as parameter
   */
  deleteSquad: async (id) => {
    const requestOptions = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      }
    };
    try {
      await fetch(`${API}/Squads/${id}`, requestOptions);
    } catch (e) {
      console.log(e.message);
    }
  },

  // API Requests for Kills

  /**
   * Fetches all the kills from the database
   * returns a JSON object containing all kills
   */
  getKills: async () => {
    const requestOptions = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      }
    };
    const kills = await fetch(`${API}/Kills`, requestOptions);
    return kills.json();
  },

  /**
   * Posts a new kill
   * Takes the position and victimname as parameters
   */
  postKill: async (latLong, victim) => {
    const players = await APIService.getPlayers();
    const player = players.filter((p) => p.biteCode === victim)[0];
    const time = moment().format();
    const user = JSON.parse(localStorage.getItem("user"));
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + KeycloakService.getToken()
      },
      body: JSON.stringify({
        timeOfDeath: time,
        latitude: latLong.latitude,
        longitude: latLong.longitude,
        killerId: user.playerId,
        victimName: player.username,
        gameId: user.gameId
      })
    };
    await fetch(`${API}/Kills`, requestOptions);
  }
};

export default APIService;
