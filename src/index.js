import reportWebVitals from "./reportWebVitals";
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App/App";
import KeycloakService from "./services/KeycloakService";
import { Provider } from "react-redux";
import store from "./store";
import Loader from "./features/Loader/Loader";

// Temporary render while keycloak initialises.
ReactDOM.render(<Loader height="100vh" />, document.getElementById("root"));

KeycloakService.initKeycloak(() => {
  // Render the actual application.
  ReactDOM.render(
    <React.StrictMode>
      <Provider store={store}>
        <App />
      </Provider>
    </React.StrictMode>,
    document.getElementById("root")
  );
});

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
